package kz.attractor.application;

import kz.attractor.application.DTO.users.CreateUserDTO;
import kz.attractor.application.DTO.users.UpdateUserDTO;
import kz.attractor.application.exceptions.DuplicateException;
import kz.attractor.application.exceptions.MaxDataException;
import kz.attractor.application.models.Authority;
import kz.attractor.application.models.Child;
import kz.attractor.application.models.User;
import kz.attractor.application.repositories.UserRepository;
import kz.attractor.application.services.AuthorityService;
import kz.attractor.application.services.EmailService;
import kz.attractor.application.services.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private AuthorityService authorityService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private EmailService emailService;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() {
        CreateUserDTO createUserDTO = CreateUserDTO.builder()
                .username("testuser")
                .firstname("Test")
                .lastname("User")
                .phone("1234567890")
                .password("testpassword")
                .build();

        when(userRepository.findAll()).thenReturn(Collections.emptyList());
        when(authorityService.findAllByAuthority("ROLE_USER")).thenReturn(Collections.singletonList(Authority.builder().authority("ROLE_USER").build()));
        when(passwordEncoder.encode(createUserDTO.getPassword())).thenReturn("encodedPassword");

        assertDoesNotThrow(() -> userService.create(createUserDTO));

        verify(userRepository, times(1)).save(any());
        verify(emailService, times(1)).sendEmail(any(), any(), any());
    }

    @Test
    public void testCreate_DuplicateUsername() {
        CreateUserDTO createUserDTO = CreateUserDTO.builder()
                .username("testuser")
                .firstname("Test")
                .lastname("User")
                .phone("1234567890")
                .password("testpassword")
                .build();

        when(userRepository.findAll()).thenReturn(Collections.singletonList(User.builder().username("testuser").build()));

        assertThrows(DuplicateException.class, () -> userService.create(createUserDTO));

        verify(userRepository, never()).save(any());
        verify(emailService, never()).sendEmail(any(), any(), any());
    }

    @Test
    public void testFindByUsername() {
        String username = "testuser";
        User user = User.builder().username(username).build();
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));

        assertEquals(user, userService.findByUsername(username));
    }

    @Test
    public void testFindByUsername_UserNotFound() {
        String username = "nonexistentuser";
        when(userRepository.findByUsername(username)).thenReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> userService.findByUsername(username));
    }

    @Test
    public void testUpdateUserInfo() {
        String username = "testuser";
        UpdateUserDTO updateUserDTO = UpdateUserDTO.builder()
                .firstname("Updated")
                .lastname("User")
                .phone("1234567890")
                .photoUrl("newUrl")
                .build();

        User existingUser = User.builder()
                .username(username)
                .build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(existingUser));
        when(userRepository.save(any())).thenReturn(existingUser);

        userService.updateUserInfo(username, updateUserDTO);

        assertEquals(updateUserDTO.getFirstname(), existingUser.getFirstname());
        assertEquals(updateUserDTO.getLastname(), existingUser.getLastname());
        assertEquals(updateUserDTO.getPhone(), existingUser.getPhone());
        assertEquals(updateUserDTO.getPhotoUrl(), existingUser.getPhotoUrl());
    }

    @Test
    public void testUpdateChildren() {
        String username = "testuser";
        Child child = Child.builder().firstname("Child").build();
        User existingUser = User.builder().username(username).build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(existingUser));
        when(userRepository.save(any())).thenReturn(existingUser);

        userService.updateChildren(username, child);

        assertTrue(existingUser.getChildren().contains(child));
    }

    @Test
    public void testUpdateChildren_MaxChildrenReached() {
        String username = "testuser";
        Child child = Child.builder().firstname("Child").build();
        User existingUser = User.builder()
                .username(username)
                .children(Collections.nCopies(10, new Child()))
                .build();

        when(userRepository.findByUsername(username)).thenReturn(Optional.of(existingUser));

        assertThrows(MaxDataException.class, () -> userService.updateChildren(username, child));
    }

    @Test
    public void testGetUserById_UserNotFound() {
        Long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> userService.getUserById(userId));
    }

}
