package kz.attractor.application.repositories;

import kz.attractor.application.models.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findByTopicIdAndIsModeratedOrderByCreatedAtDesc(Long topicId, String isModerated);
    List<Question> findByIsModeratedOrderByCreatedAtDesc(String isModerated);

}

