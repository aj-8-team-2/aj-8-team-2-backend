package kz.attractor.application.repositories;

import kz.attractor.application.models.Child;
import kz.attractor.application.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    List<Child> findAllById(Long id);
}
