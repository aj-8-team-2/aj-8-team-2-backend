package kz.attractor.application.repositories;


import kz.attractor.application.models.ClubBranch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClubBranchRepository extends JpaRepository<ClubBranch, Long> {
    List<ClubBranch> findAllByBranchId(Long branchId);
    List<ClubBranch> findAllByClubId(Long clubId);
    List<ClubBranch> findAllByClubCategoryId(Long categoryId);
    List<ClubBranch> findAllByClubSubcategoryId(Long subcategoryId);
}
