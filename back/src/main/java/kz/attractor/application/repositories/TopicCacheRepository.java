package kz.attractor.application.repositories;

import kz.attractor.application.models.TopicCache;
import org.springframework.data.repository.CrudRepository;

public interface TopicCacheRepository extends CrudRepository<TopicCache, String> {
}