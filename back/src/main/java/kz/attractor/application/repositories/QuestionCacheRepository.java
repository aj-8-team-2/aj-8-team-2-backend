package kz.attractor.application.repositories;

import kz.attractor.application.models.QuestionCache;
import org.springframework.data.repository.CrudRepository;

public interface QuestionCacheRepository extends CrudRepository<QuestionCache, String> {
}