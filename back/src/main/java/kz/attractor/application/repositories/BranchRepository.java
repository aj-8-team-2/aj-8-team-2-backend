package kz.attractor.application.repositories;

import kz.attractor.application.models.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch, Long> {
    List<Branch> findAllByNameContainingIgnoreCase(String name);
    List<Branch> findAllByOrderByRatingDesc();
    List<Branch> findAllByOrderByNameAsc();
    List<Branch> findAllByActiveOrderByNameAsc(boolean active);
}
