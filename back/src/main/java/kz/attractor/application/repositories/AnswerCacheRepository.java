package kz.attractor.application.repositories;

import kz.attractor.application.models.AnswerCache;
import org.springframework.data.repository.CrudRepository;

public interface AnswerCacheRepository extends CrudRepository<AnswerCache, String> {
}