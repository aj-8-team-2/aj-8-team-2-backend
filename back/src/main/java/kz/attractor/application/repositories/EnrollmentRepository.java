package kz.attractor.application.repositories;

import kz.attractor.application.models.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
    List<Enrollment> findAllByChildId(Long childId);
    boolean existsByChildIdAndBranchId(Long childId, Long BranchId);
}
