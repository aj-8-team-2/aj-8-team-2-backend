package kz.attractor.application.repositories;

import kz.attractor.application.models.TestFree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestFreeRepository extends JpaRepository<TestFree, Long> {
    TestFree findByChildId(Long id);

    void deleteByChildId(Long id);
}
