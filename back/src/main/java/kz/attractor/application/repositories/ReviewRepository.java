package kz.attractor.application.repositories;

import kz.attractor.application.models.Branch;
import kz.attractor.application.models.Review;
import kz.attractor.application.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ReviewRepository extends JpaRepository<Review, Long> {
    List<Review> findByBranchId(Long clubId);

    long countByUserAndBranch(User user, Branch branch);
}
