package kz.attractor.application.repositories;


import kz.attractor.application.models.Authority;
import kz.attractor.application.services.AuthorityService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    List<Authority> findAllByAuthority(String authority);
}
