package kz.attractor.application.controllers;

import kz.attractor.application.DTO.TopicDTO;
import kz.attractor.application.services.TopicService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("topics")
public class TopicController {
    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }
    @GetMapping
    public List<TopicDTO> getAllTopics() {
        return topicService.findAllTopics();
    }
}

