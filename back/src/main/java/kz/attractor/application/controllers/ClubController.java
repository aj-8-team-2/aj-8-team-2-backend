package kz.attractor.application.controllers;

import kz.attractor.application.DTO.clubs.CreateBranchDTO;
import kz.attractor.application.DTO.clubs.CreateClubDTO;
import kz.attractor.application.DTO.clubs.GetClubDTO;
import kz.attractor.application.models.Club;
import kz.attractor.application.services.clubs.ClubService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clubs")
public class ClubController {
    private final ClubService clubService;

    public ClubController(ClubService clubService) {
        this.clubService = clubService;
    }

    @GetMapping
    public List<GetClubDTO> getAllBranches() {
        return clubService.getAllOrderByName();
    }

    @PostMapping()
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public void addClub(@RequestBody CreateClubDTO createClubDTO) {
        clubService.add(createClubDTO);
    }

    @PostMapping("/{id}/addBranch")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public void addBranch(@PathVariable String id, @RequestBody CreateBranchDTO createBranchDTO) {
        clubService.addBranch(id, createBranchDTO);
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasRole('ROLE_MANAGER')")
    public void deleteClub(@PathVariable Long id) {
        clubService.delete(id);
    }
}
