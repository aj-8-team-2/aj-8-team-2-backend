package kz.attractor.application.controllers;

import jakarta.servlet.http.HttpSession;
import kz.attractor.application.models.Child;
import kz.attractor.application.models.User;
import kz.attractor.application.services.ChildService;
import kz.attractor.application.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/children")
@RequiredArgsConstructor
public class ChildController {
    private final ChildService childService;
    private final UserService userService;

    @GetMapping("/{id}")
    public Child getChildById(@PathVariable Long id) {
        return childService.getChildById(id);
    }

    @PostMapping("/{username}")
    public ResponseEntity<?> addChild(@PathVariable String username, @RequestBody Child createChildDTO) {
        childService.addChild(username, createChildDTO);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public void deleteChild(@PathVariable Long id){
        childService.deleteChild(id);
    }
    @GetMapping("/byUser/{username}")
    public ResponseEntity<List<Child>> getChildrenForCurrentUser(@PathVariable String username) {
        User user = userService.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }

        List<Child> children = childService.findAllChildrenByUserId(user.getId());

        return ResponseEntity.ok(children);
    }
}