package kz.attractor.application.controllers;

import kz.attractor.application.DTO.QuestionDTO;
import kz.attractor.application.models.Question;
import kz.attractor.application.services.QuestionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("questions")
public class QuestionController {
    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping
    public ResponseEntity<?> createQuestion(@RequestBody QuestionDTO questionDTO) {
        try {
            questionService.save(questionDTO);
            return new ResponseEntity<>("Вопрос успешно добавлен", HttpStatus.CREATED);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{topicId}")
    public ResponseEntity<List<QuestionDTO>> getQuestionsByTopic(@PathVariable Long topicId) {
        List<QuestionDTO> questions = questionService.findByTopicIdOrderByCreatedAtDesc(topicId);
        return ResponseEntity.ok(questions);
    }

    @GetMapping("/moderation")
    public List<Question> getQuestionsForModeration() {
        return questionService.findQuestionsForModeration();
    }

    @PutMapping("/{questionId}/moderate")
    public ResponseEntity<?> moderateQuestion(@PathVariable Long questionId) {
        try {
            questionService.moderateQuestion(questionId);
            return ResponseEntity.ok("Вопрос успешно отмодерирован");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @DeleteMapping("/{questionId}/delete")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long questionId) {
        try {
            questionService.deleteQuestion(questionId);
            return ResponseEntity.ok("Вопрос успешно удалён");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
