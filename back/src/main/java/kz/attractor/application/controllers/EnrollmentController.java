package kz.attractor.application.controllers;

import kz.attractor.application.DTO.EnrollmentRequestDTO;
import kz.attractor.application.models.Branch;
import kz.attractor.application.services.EnrollmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("enrollments")
public class EnrollmentController {

    private final EnrollmentService enrollmentService;

    public EnrollmentController(EnrollmentService enrollmentService) {
        this.enrollmentService = enrollmentService;
    }

    @GetMapping("/{id}")
    public List<Branch> getAllClubsByChildId(@PathVariable Long id){
       return enrollmentService.getClubsByChildId(id);
    }

    @PostMapping
    public ResponseEntity<?> enrollChild(@RequestBody EnrollmentRequestDTO request) {
        try {
            enrollmentService.enrollChildToClub(request.getChildId(), request.getClubId());
            return ResponseEntity.ok().body("Ребенок успешно записан в клуб!");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Произошла ошибка при записи ребенка в клуб: " + e.getMessage());
        }
    }
}
