package kz.attractor.application.controllers;

import kz.attractor.application.DTO.clubs.UpdateBranchDTO;
import kz.attractor.application.models.Branch;
import kz.attractor.application.services.clubs.BranchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/branches")
public class BranchController {
    private final BranchService branchService;

    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    @GetMapping
    public List<Branch> getAllBranches() {
        return branchService.getAllBranchesSortedByRating();
    }

    @GetMapping("/orderByName")
    public List<Branch> getAllBranchesOrderedByName() {
        return branchService.getAllByOrderByNameAsc();
    }

    @GetMapping("/filterByRelevance")
    public List<Branch> getAllBranchesByActiveOrderedByName(@RequestParam Boolean active) {
        return branchService.getAllByActiveOrderByNameAsc(active);
    }

    @GetMapping("/{id}")
    public Branch getBranchById(@PathVariable Long id) {
        return branchService.getBranchById(id);
    }

    @PutMapping("/{id}")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public ResponseEntity<?> editBranch(@PathVariable Long id, @RequestBody UpdateBranchDTO updateBranchDTO) {
        try {
            branchService.edit(id, updateBranchDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/deactivate/{id}")
//    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public ResponseEntity<?> deactivateBranch(@PathVariable Long id) {
        try {
            branchService.deactivate(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
//    @PreAuthorize("hasRole('ROLE_MANAGER')")
    public void deleteBranch(@PathVariable Long id) {
        branchService.delete(id);
    }

    @GetMapping("/filterByCategory")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Branch> filterBranchesByCategory(@RequestParam(required = false) String categoryId) {
        List<Branch> filteredBranches = branchService.filterBranchesByCategory(categoryId);

        return branchService.sortByRating(filteredBranches);
    }


    @GetMapping("/filterBySubcategory")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Branch> filterBranchesBySubcategory(@RequestParam(required = false) String subcategoryId) {
        List<Branch> filteredBranches = branchService.filterBranchesBySubcategory(subcategoryId);

        return branchService.sortByRating(filteredBranches);
    }

    @GetMapping("/searchByName")
//    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Branch> searchBranchesByName(@RequestParam(required = false) String name) {
        List<Branch> filteredBranches = branchService.searchBranchesByName(name);

        return branchService.sortByRating(filteredBranches);
    }


}

