package kz.attractor.application.controllers;

import kz.attractor.application.DTO.users.CreateUserDTO;
import kz.attractor.application.DTO.users.GetUserDTO;
import kz.attractor.application.DTO.users.UpdateUserDTO;
import kz.attractor.application.models.User;
import kz.attractor.application.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping(name = "/users", method = RequestMethod.POST)
    public void create(@RequestBody @Valid CreateUserDTO createUserDTO) {
        service.create(createUserDTO);
    }

    @GetMapping("/{username}")
    public ResponseEntity<GetUserDTO> get(@PathVariable String username) {
        try {
            User user = service.findByUsername(username);
            return ResponseEntity
                    .ok()
                    .body(GetUserDTO.map(user));
        }
        catch (IllegalArgumentException exception) {
            return ResponseEntity
                    .notFound()
                    .build();
        }
    }

    @PutMapping("/{username}")
    public void updateUserInfo(@PathVariable String username, @RequestBody @Valid UpdateUserDTO updateUserDTO) {
        service.updateUserInfo(username, updateUserDTO);
    }
}

