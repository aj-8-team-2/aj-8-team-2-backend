package kz.attractor.application.controllers;

import kz.attractor.application.DTO.AdvertisementDTO;
import kz.attractor.application.DTO.ImageDTO;
import kz.attractor.application.models.Advertisement;
import kz.attractor.application.models.Image;
import kz.attractor.application.services.AdvertisementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/advertisements")
public class AdvertisementController {

    private final AdvertisementService advertisementService;

    @Autowired
    public AdvertisementController(AdvertisementService advertisementService) {
        this.advertisementService = advertisementService;
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public ResponseEntity<List<Advertisement>> getAllAdvertisements() {
        List<Advertisement> advertisements = advertisementService.getAllAdvertisements();
        return new ResponseEntity<>(advertisements, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Advertisement> getAdvertisementById(@PathVariable Long id) {
        Optional<Advertisement> advertisement = advertisementService.getAdvertisementById(id);
        return advertisement.map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_MANAGER')")
    public ResponseEntity<Advertisement> createAdvertisement(@RequestBody AdvertisementDTO advertisementDTO) {
        Advertisement createdAdvertisement = advertisementService.createAdvertisement(advertisementDTO);
        return new ResponseEntity<>(createdAdvertisement, HttpStatus.CREATED);
    }


    @PutMapping("/update/{id}")
    public ResponseEntity<Advertisement> updateAdvertisement(@PathVariable Long id,
                                                             @RequestBody Advertisement updatedAdvertisement)
                                                                throws ChangeSetPersister.NotFoundException {
        Advertisement updated = advertisementService.updateAdvertisement(id, updatedAdvertisement);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteAdvertisement(@PathVariable Long id) {
        advertisementService.deleteAdvertisement(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/sortedByPriority")
    public List<Advertisement> getAdvertisementsSortedByPriority() {
        return advertisementService.getAdvertisementsSortedByPriority();
    }

    @PostMapping("/create/images/{id}")
    public ResponseEntity<?> uploadImage(@PathVariable Long id, @Validated @ModelAttribute ImageDTO imageDTO) {
        advertisementService.addAdvertImage(imageDTO, id);
        return ResponseEntity.ok("Картинки успешно добавлены");
    }

}

