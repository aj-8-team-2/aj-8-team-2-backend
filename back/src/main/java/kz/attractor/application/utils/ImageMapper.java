package kz.attractor.application.utils;

import kz.attractor.application.DTO.ImageDTO;
import kz.attractor.application.models.Image;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    ImageDTO toDto(Image image);
    List<ImageDTO> toDto(List<Image> images);
    Image toEntity(ImageDTO dto);

}
