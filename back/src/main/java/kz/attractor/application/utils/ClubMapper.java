package kz.attractor.application.utils;

import kz.attractor.application.DTO.clubs.CreateBranchDTO;
import kz.attractor.application.models.Branch;


public class ClubMapper {
    public static Branch mapClubBranch(CreateBranchDTO dto) {
        return new Branch(dto.getName(), dto.getAddress(), dto.getPhoneNumber(), dto.getService(),
                dto.isMainBranch(), dto.getTeachers(), dto.getSchedule(), dto.getInstagram());
    }
}
