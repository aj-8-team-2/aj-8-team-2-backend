package kz.attractor.application.enums;

import java.util.HashMap;
import java.util.Map;

public enum TestFreeEnum {
    BASKETBALL(1, "Баскетбол", "src/main/java/kz/attractor/application/images/freeTests/basketball.png"),
    BICYCLING(2, "Велосипедный спорт", "src/main/java/kz/attractor/application/images/freeTests/bicycling.png"),
    BORBA(3, "Борьба", "src/main/java/kz/attractor/application/images/freeTests/borba.png"),
    BOXING(4, "Бокс", "src/main/java/kz/attractor/application/images/freeTests/boxing.png"),
    CHESS(5, "Шахматы", "src/main/java/kz/attractor/application/images/freeTests/chess.png"),
    DOUBLE_TENNIS(6, "Парный теннис", "src/main/java/kz/attractor/application/images/freeTests/double_tennis.png"),
    EDINOBORSTVO(7, "Единоборство", "src/main/java/kz/attractor/application/images/freeTests/edinoborstvo.png"),
    FOOTBALL(8, "Футбол", "src/main/java/kz/attractor/application/images/freeTests/football.png"),
    GROUP_GYMNASTIC(9, "Групповая гимнастика", "src/main/java/kz/attractor/application/images/freeTests/group_gymnastic.png"),
    GYMNASTIC(10, "Гимнастика", "src/main/java/kz/attractor/application/images/freeTests/gymnastic.png"),
    HANDBALL(11, "Гандбол", "src/main/java/kz/attractor/application/images/freeTests/handball.png"),
    HOCKEY(12, "Хоккей", "src/main/java/kz/attractor/application/images/freeTests/hockey.jpeg"),
    SKATING(13, "Фигурное катание", "src/main/java/kz/attractor/application/images/freeTests/skating.png"),
    SKIING(14, "Лыжный спорт", "src/main/java/kz/attractor/application/images/freeTests/skiing.png"),
    SPORT_DANCE(15, "Спортивные танцы", "src/main/java/kz/attractor/application/images/freeTests/sport_dance.png"),
    SWIMMING(16, "Плавание", "src/main/java/kz/attractor/application/images/freeTests/swimming.png"),
    SYNHRONIZED_SWIMMING(17, "Синхронное плавание", "src/main/java/kz/attractor/application/images/freeTests/synchronizwd_swimming.png"),
    TENNIS_INDIVIDUAL(18, "Теннис", "src/main/java/kz/attractor/application/images/freeTests/tennis_individual.png"),
    VOLEYBALL(19, "Волейбол", "src/main/java/kz/attractor/application/images/freeTests/voleyball.png"),
    WATER_POLO(20, "Водное поло", "src/main/java/kz/attractor/application/images/freeTests/water_polo.png");

    private Integer id;
    private String title;
    private String path;

    TestFreeEnum(Integer id, String title, String path) {
        this.id = id;
        this.title = title;
        this.path = path;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPath() {
        return path;
    }

    private static final Map<Integer, TestFreeEnum> BY_ID = new HashMap<>();

    static {
        for (TestFreeEnum e : values()) {
            BY_ID.put(e.id, e);
        }
    }

    public static TestFreeEnum valueOf(int id) {
        return BY_ID.get(id);
    }
}
