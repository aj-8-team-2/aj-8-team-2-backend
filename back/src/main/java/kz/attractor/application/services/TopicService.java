package kz.attractor.application.services;

import kz.attractor.application.DTO.TopicDTO;

import java.util.List;

public interface TopicService {
    List<TopicDTO> findAllTopics();
}
