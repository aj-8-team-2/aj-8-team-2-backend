package kz.attractor.application.services;

import kz.attractor.application.DTO.AnswerDTO;

import java.util.List;

public interface AnswerService {
    void save(AnswerDTO answerDTO);
    List<AnswerDTO> findAnswersByQuestionId(Long questionId);
}
