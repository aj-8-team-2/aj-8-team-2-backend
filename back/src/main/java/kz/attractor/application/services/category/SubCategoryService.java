package kz.attractor.application.services.category;

import kz.attractor.application.DTO.categories.CreateSubCategoryDTO;
import kz.attractor.application.models.SubCategory;

import java.util.List;

public interface SubCategoryService {
    List<SubCategory> getAllSubcategories();
    SubCategory getSubcategoryById(Long id);
    void addSubcategory(CreateSubCategoryDTO subcategoryDTO);

    void editSubcategory(Long id, SubCategory updatedSubcategory);

    void deleteSubcategory(Long id);

    List<SubCategory> getSubcategoriesByCategoryId(Long categoryId);
}
