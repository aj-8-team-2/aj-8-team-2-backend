package kz.attractor.application.services.clubs;

import kz.attractor.application.models.Branch;
import kz.attractor.application.models.Club;
import kz.attractor.application.models.ClubBranch;

import java.util.List;

public interface ClubBranchService {
    void add(Club club, Branch branch);
    void delete(Long id);
    List<ClubBranch> findByBranchId(Long branchId);
    List<ClubBranch> findAllByClubId(Long clubId);
    List<ClubBranch> getAllByCategoryId(Long categoryId);
    List<ClubBranch> getAllBySubcategoryId(Long subcategoryId);

}
