package kz.attractor.application.services.category.impl;

import kz.attractor.application.DTO.categories.CreateSubCategoryDTO;
import kz.attractor.application.models.SubCategory;
import kz.attractor.application.repositories.SubCategoryRepository;
import kz.attractor.application.services.category.CategoryService;
import kz.attractor.application.services.category.SubCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    private final SubCategoryRepository subcategoryRepository;
    private final CategoryService categoryService;

    public SubCategoryServiceImpl(SubCategoryRepository subcategoryRepository, CategoryService categoryService) {
        this.subcategoryRepository = subcategoryRepository;
        this.categoryService = categoryService;
    }

    @Override
    public List<SubCategory> getAllSubcategories() {
        return subcategoryRepository.findAll();
    }

    @Override
    public SubCategory getSubcategoryById(Long id) {
        try {
            return subcategoryRepository.findById(id)
                    .orElseThrow(() -> new NoSuchElementException("Подкатегория с ID " + id + " не найдена"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void addSubcategory(CreateSubCategoryDTO subcategoryDTO) {
        SubCategory subcategory = SubCategory
                .builder()
                .name(subcategoryDTO.getName())
                .category(categoryService.getCategoryById(subcategoryDTO.getCategoryId()))
                .build();

        subcategoryRepository.save(subcategory);
    }

    @Override
    public void editSubcategory(Long id, SubCategory updatedSubcategory) {
        SubCategory existingSubCategory = subcategoryRepository.findById(id).orElseThrow();

        if (updatedSubcategory.getName() != null) {
            existingSubCategory.setName(updatedSubcategory.getName());
        }

        subcategoryRepository.save(existingSubCategory);
    }


    @Override
    public void deleteSubcategory(Long id) {
        subcategoryRepository.deleteById(id);
    }

    @Override
    public List<SubCategory> getSubcategoriesByCategoryId(Long categoryId) {
        return subcategoryRepository.findByCategoryId(categoryId);
    }
}
