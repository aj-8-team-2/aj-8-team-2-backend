package kz.attractor.application.services.clubs.impl;

import kz.attractor.application.DTO.clubs.CreateBranchDTO;
import kz.attractor.application.DTO.clubs.UpdateBranchDTO;
import kz.attractor.application.models.Branch;
import kz.attractor.application.models.ClubBranch;
import kz.attractor.application.repositories.BranchRepository;
import kz.attractor.application.services.clubs.BranchService;
import kz.attractor.application.services.clubs.ClubBranchService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    private final ClubBranchService clubBranchService;

    public BranchServiceImpl(BranchRepository branchRepository, ClubBranchService clubBranchService) {
        this.branchRepository = branchRepository;
        this.clubBranchService = clubBranchService;
    }

    @Override
    public List<Branch> getAllBranches() {
        return branchRepository.findAll();
    }

    @Override
    public Branch getBranchById(Long id) {
        Optional<Branch> branch = branchRepository.findById(id);

        if (branch.isEmpty())
        {
            throw new NoSuchElementException("Branch with ID %s doesn't exist!".formatted(branch));
        }

        return branch.get();
    }

    @Override
    public void add(CreateBranchDTO createBranchDTO) {
        Branch branch = Branch
                            .builder()
                            .name(createBranchDTO.getName())
                            .address(createBranchDTO.getAddress())
                            .service(createBranchDTO.getService())
                            .phoneNumber(createBranchDTO.getPhoneNumber())
                            .mainBranch(createBranchDTO.isMainBranch())
                            .teachers(createBranchDTO.getTeachers())
                            .schedule(createBranchDTO.getSchedule())
                            .build();

        branchRepository.save(branch);
    }

    @Override
    public void delete(Long id) {
        List<ClubBranch> clubBranches = clubBranchService.findByBranchId(id);
        clubBranches.forEach(cb -> clubBranchService.delete(cb.getId()));
        branchRepository.deleteById(id);
    }

    @Override
    public void deactivate(Long id) {
        Branch branch = getBranchById(id);
        branch.setActive(false);

        branchRepository.save(branch);
    }

    @Override
    public void edit(Long branchId, UpdateBranchDTO updateBranchDTO) {
        Branch branch = getBranchById(branchId);

        if (updateBranchDTO.getService() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setService(updateBranchDTO.getService());
        }
        if (updateBranchDTO.getAddress() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setAddress(updateBranchDTO.getAddress());
        }
        if (updateBranchDTO.getName() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setName(updateBranchDTO.getName());
        }
        if (updateBranchDTO.getInstagram() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setInstagram(updateBranchDTO.getInstagram());
        }
        if (updateBranchDTO.getPhoneNumber() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setPhoneNumber(updateBranchDTO.getPhoneNumber());
        }
        if (updateBranchDTO.getTeachers() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setTeachers(updateBranchDTO.getTeachers());
        }
        if (updateBranchDTO.getSchedule() != null && !updateBranchDTO.getService().isEmpty()) {
            branch.setSchedule(updateBranchDTO.getSchedule());
        }
        if (updateBranchDTO.isMainBranch() != branch.isMainBranch()) {
            branch.setMainBranch(updateBranchDTO.isMainBranch());
        }

        branchRepository.save(branch);
    }

    @Override
    public Branch save(Branch branch) {
        return branchRepository.save(branch);
    }

    @Override
    public List<Branch> sortByRating(List<Branch> branches) {
        List<Branch> activeBranches = new ArrayList<>();

        for (Branch branch : branches) {
            if (branch.isActive()) {
                activeBranches.add(branch);
            }
        }

        activeBranches.sort(Comparator.comparingDouble(Branch::getRating).reversed());
        return activeBranches;
    }

    @Override
    public List<Branch> filterBranchesByCategory(String categoryId) {
        if (categoryId != null && !categoryId.isEmpty()) {
            List<ClubBranch> filteredClubBranches = clubBranchService.getAllByCategoryId(Long.valueOf(categoryId));
            return filteredClubBranches.stream().map(ClubBranch::getBranch).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<Branch> filterBranchesBySubcategory(String subcategoryId) {
        if (subcategoryId != null && !subcategoryId.isEmpty()) {
            List<ClubBranch> filteredClubBranches = clubBranchService.getAllBySubcategoryId(Long.valueOf(subcategoryId));
            return filteredClubBranches.stream().map(ClubBranch::getBranch).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<Branch> searchBranchesByName(String name) {
        if (name != null && !name.isEmpty()) {
            return branchRepository.findAllByNameContainingIgnoreCase(name);
        }
        return new ArrayList<>();
    }

    @Override
    public List<Branch> getAllBranchesSortedByRating() {
        return branchRepository.findAllByOrderByRatingDesc();
    }

    @Override
    public List<Branch> getAllByOrderByNameAsc() {
        return branchRepository.findAllByOrderByNameAsc();
    }

    @Override
    public List<Branch> getAllByActiveOrderByNameAsc(boolean active) {
        return branchRepository.findAllByActiveOrderByNameAsc(active);
    }
}
