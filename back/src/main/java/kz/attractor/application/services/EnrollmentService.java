package kz.attractor.application.services;

import kz.attractor.application.models.Branch;

import java.util.List;

public interface EnrollmentService {
    List<Branch> getClubsByChildId(Long childId);
    void enrollChildToClub(Long childId, Long clubId);
}
