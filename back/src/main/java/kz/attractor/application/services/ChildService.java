package kz.attractor.application.services;

import kz.attractor.application.models.Child;

import java.util.List;

public interface ChildService {
    void addChild(String username, Child childDTO);
    List<Child> getAllChildren();
    Child getChildById(Long id);
    void deleteChild(Long id);
    List<Child> findAllChildrenByUserId(Long id);
}