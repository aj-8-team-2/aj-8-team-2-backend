package kz.attractor.application.services;

import kz.attractor.application.models.News;

import java.util.List;

public interface NewsService {
    List<News> getAllNews();

    News createNews(News news);
    void deleteNews(Long id);
}
