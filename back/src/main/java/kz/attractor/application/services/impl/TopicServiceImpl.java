package kz.attractor.application.services.impl;

import kz.attractor.application.DTO.TopicDTO;
import kz.attractor.application.models.Topic;
import kz.attractor.application.models.TopicCache;
import kz.attractor.application.repositories.TopicCacheRepository;
import kz.attractor.application.repositories.TopicRepository;
import kz.attractor.application.services.TopicService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository topicRepository;
    private final TopicCacheRepository topicCacheRepository;

    public TopicServiceImpl(TopicRepository topicRepository, TopicCacheRepository topicCacheRepository) {
        this.topicRepository = topicRepository;
        this.topicCacheRepository = topicCacheRepository;
    }

    @Override
    public List<TopicDTO> findAllTopics() {
        TopicCache cachedTopics = topicCacheRepository.findById("allTopics").orElse(null);

        if (cachedTopics != null) {
            return cachedTopics.getTopics();
        }

        List<Topic> topics = topicRepository.findAll();

        List<TopicDTO> topicDTOs = topics.stream().map(topic -> new TopicDTO(topic.getId(), topic.getName(), topic.getDescription()))
                .collect(Collectors.toList());

        TopicCache topicCache = new TopicCache();
        topicCache.setId("allTopics");
        topicCache.setTopics(topicDTOs);
        topicCacheRepository.save(topicCache);

        return topicDTOs;
    }
}

