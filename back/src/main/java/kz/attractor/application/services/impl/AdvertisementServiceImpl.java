package kz.attractor.application.services.impl;

import kz.attractor.application.DTO.AdvertisementDTO;
import kz.attractor.application.DTO.ImageDTO;
import kz.attractor.application.models.Advertisement;
import kz.attractor.application.models.Image;
import kz.attractor.application.models.Priority;
import kz.attractor.application.repositories.AdvertisementRepository;
import kz.attractor.application.repositories.PriorityRepository;
import kz.attractor.application.services.AdvertisementService;
import kz.attractor.application.services.ImageService;
import kz.attractor.application.utils.ImageMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AdvertisementServiceImpl implements AdvertisementService {

    public final AdvertisementRepository advertisementRepository;
    public final PriorityRepository priorityRepository;
    public final ImageService imageService;
    public final ImageMapper imageMapper;


    @Override
    public List<Advertisement> getAllAdvertisements() {
        return advertisementRepository.findAll();
    }

    @Override
    public Optional<Advertisement> getAdvertisementById(Long id) {
        return advertisementRepository.findById(id);
    }

    @Override
    public Advertisement createAdvertisement(AdvertisementDTO advertisementDTO) {
        Advertisement advertisement = new Advertisement();
//        advertisement.setImage(advertisementDTO.getImage());
        advertisement.setLink(advertisementDTO.getLink());
        advertisement.setName(advertisementDTO.getName());

        Priority priority = priorityRepository.findById(advertisementDTO.getPriorityID())
                .orElseThrow(() -> new IllegalArgumentException("Invalid priorityID"));

        advertisement.setPriority(priority);

        return advertisementRepository.save(advertisement);
    }

    @Override
    public void addAdvertImage(ImageDTO imageDTO, Long id) {
        try {
            Advertisement advertisement = advertisementRepository.findById(id)
                    .orElseThrow(ChangeSetPersister.NotFoundException::new);

            Image image = imageMapper.toEntity(imageDTO);
            String fileName = imageService.upload(image);
            advertisement.getImages().add(fileName);

            advertisementRepository.save(advertisement);
        } catch (ChangeSetPersister.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Advertisement updateAdvertisement(Long id, Advertisement updatedAdvertisement) throws ChangeSetPersister.NotFoundException {
        Advertisement existingAdvertisement = advertisementRepository.findById(id)
                .orElseThrow(ChangeSetPersister.NotFoundException::new);

        existingAdvertisement.setName(updatedAdvertisement.getName());
//        existingAdvertisement.setImage(updatedAdvertisement.getImage());
        existingAdvertisement.setLink(updatedAdvertisement.getLink());
        existingAdvertisement.setPriority(updatedAdvertisement.getPriority());

        return advertisementRepository.save(existingAdvertisement);
    }

    @Override
    public void deleteAdvertisement(Long id) {
        advertisementRepository.deleteById(id);
    }

    @Override
    public List<Advertisement> getAdvertisementsSortedByPriority() {
        return advertisementRepository.findByOrderByPriorityDesc();
    }

//    public List<Advertisement> getAdvertisementsSortedByPriorityAndRating() {
//        List<Advertisement> advertisements = advertisementRepository.findByOrderByPriorityDesc();
//
//        return advertisements.stream()
//                .sorted(Comparator.comparing(Advertisement::getRating).reversed())
//                .collect(Collectors.toList());
//    }

}
