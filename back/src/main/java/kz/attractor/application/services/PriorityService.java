package kz.attractor.application.services;

import kz.attractor.application.models.Priority;

import java.util.List;

public interface PriorityService {
    List<Priority> getAllPriorities();

    Priority getPriorityById(Long id);

    Priority createPriority(Priority priority);

    Priority updatePriority(Long id, Priority priority);

    void deletePriority(Long id);
}
