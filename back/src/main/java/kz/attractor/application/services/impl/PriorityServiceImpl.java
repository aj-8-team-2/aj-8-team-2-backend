package kz.attractor.application.services.impl;

import kz.attractor.application.models.Priority;
import kz.attractor.application.repositories.PriorityRepository;
import kz.attractor.application.services.PriorityService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PriorityServiceImpl implements PriorityService {

    private final PriorityRepository priorityRepository;

    public PriorityServiceImpl(PriorityRepository priorityRepository) {
        this.priorityRepository = priorityRepository;
    }

    @Override
    public List<Priority> getAllPriorities() {
        return priorityRepository.findAll();
    }

    @Override
    public Priority getPriorityById(Long id) {
        Optional<Priority> optionalPriority = priorityRepository.findById(id);
        return optionalPriority.orElse(null);
    }

    @Override
    public Priority createPriority(Priority priority) {
        // Add any additional business logic or validation before saving
        return priorityRepository.save(priority);
    }

    @Override
    public Priority updatePriority(Long id, Priority updatedPriority) {
        Optional<Priority> optionalPriority = priorityRepository.findById(id);

        if (optionalPriority.isPresent()) {
            Priority existingPriority = optionalPriority.get();

            // Update the fields you want to allow modification
            existingPriority.setPriority(updatedPriority.getPriority());

            // Add any additional business logic or validation before saving
            return priorityRepository.save(existingPriority);
        } else {
            // Handle not found case
            return null;
        }
    }

    @Override
    public void deletePriority(Long id) {
        priorityRepository.deleteById(id);
    }
}
