package kz.attractor.application.services.category;

import kz.attractor.application.DTO.categories.CreateCategoryDTO;
import kz.attractor.application.models.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();
    Category getCategoryById(Long id);
    void addCategory(CreateCategoryDTO categoryDTO);

    void editCategory(Long id, Category updatedCategory);

    void deleteCategory(Long id);
}

