package kz.attractor.application.services;


import jakarta.servlet.http.HttpSession;
import kz.attractor.application.DTO.users.CreateUserDTO;
import kz.attractor.application.DTO.users.UpdateUserDTO;
import kz.attractor.application.models.Child;
import kz.attractor.application.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
    List<Child> getAllChildrenByUserId(Long userId);
    void updateChildren(String username, Child child);
    void create(CreateUserDTO createUserDTO);
    void updateUserInfo(String username, UpdateUserDTO updateUserDTO);

    User getUserById(Long userId);
}
