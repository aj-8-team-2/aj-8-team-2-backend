package kz.attractor.application.services.impl;


import kz.attractor.application.DTO.QuestionDTO;
import kz.attractor.application.models.Question;
import kz.attractor.application.models.QuestionCache;
import kz.attractor.application.models.Topic;
import kz.attractor.application.models.User;
import kz.attractor.application.repositories.QuestionCacheRepository;
import kz.attractor.application.repositories.QuestionRepository;
import kz.attractor.application.repositories.TopicRepository;
import kz.attractor.application.services.QuestionService;
import kz.attractor.application.services.UserService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final QuestionCacheRepository questionCacheRepository;
    private final TopicRepository topicRepository;
    private final UserService userService;

    public QuestionServiceImpl(QuestionRepository questionRepository,
                               QuestionCacheRepository questionCacheRepository,
                               TopicRepository topicRepository,
                               UserService userService) {
        this.questionRepository = questionRepository;
        this.questionCacheRepository = questionCacheRepository;
        this.topicRepository = topicRepository;
        this.userService = userService;
    }

    @Override
    public void save(QuestionDTO questionDTO) {

        Topic topic = topicRepository.findById(questionDTO.getTopicId())
                .orElseThrow(() -> new IllegalArgumentException("Категория с ID " + questionDTO.getTopicId() + " не найдена"));

        String username = questionDTO.getUsername();
        User user = userService.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }

        Question question = Question.builder()
                .user(user)
                .topic(topic)
                .isModerated("NO")
                .text("Вопрос от " + user.getUsername().toUpperCase() + ": " + questionDTO.getText())
                .createdAt(LocalDateTime.now())
                .build();

        try {
            questionCacheRepository.deleteById(questionDTO.getTopicId().toString());
        } catch (Exception exception) {
            System.err.println("Ошибка при удалении данных из кэша: " + exception.getMessage());
        }

        questionRepository.save(question);
    }

    public List<QuestionDTO> findByTopicIdOrderByCreatedAtDesc(Long topicId) {

        Optional<QuestionCache> cachedQuestionsOpt = questionCacheRepository.findById(topicId.toString());

        if (cachedQuestionsOpt.isPresent()) {
            return cachedQuestionsOpt.get().getQuestions();
        } else {
            List<Question> questions = questionRepository.findByTopicIdAndIsModeratedOrderByCreatedAtDesc(topicId, "YES");
            List<QuestionDTO> questionDTOs = questions.stream()
                    .map(question -> new QuestionDTO(question.getId(), question.getUser().getUsername(), question.getTopic().getId(), question.getText()))
                    .collect(Collectors.toList());

            QuestionCache questionCache = new QuestionCache();
            questionCache.setId(topicId.toString());
            questionCache.setQuestions(questionDTOs);
            questionCacheRepository.save(questionCache);

            return questionDTOs;
        }
    }
    @Override
    public List<Question> findQuestionsForModeration() {
        return questionRepository.findByIsModeratedOrderByCreatedAtDesc("NO");
    }
    @Override
    public void moderateQuestion(Long questionId) {
        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new IllegalArgumentException("Вопрос не найден"));
        question.setIsModerated("YES");
        questionRepository.save(question);

        String cacheId = question.getTopic().getId().toString();

        try {
            questionCacheRepository.deleteById(cacheId);
        } catch (Exception exception) {
            System.err.println("Ошибка при удалении данных из кэша: " + exception.getMessage());
        }
    }

    @Override
    public void deleteQuestion(Long questionId) {
        questionRepository.deleteById(questionId);
    }
}

