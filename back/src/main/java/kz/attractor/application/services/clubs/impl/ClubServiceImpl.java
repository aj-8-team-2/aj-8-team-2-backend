package kz.attractor.application.services.clubs.impl;

import kz.attractor.application.DTO.clubs.CreateBranchDTO;
import kz.attractor.application.DTO.clubs.CreateClubDTO;
import kz.attractor.application.DTO.clubs.GetClubDTO;
import kz.attractor.application.models.Branch;
import kz.attractor.application.models.Club;
import kz.attractor.application.models.ClubBranch;
import kz.attractor.application.repositories.ClubRepository;
import kz.attractor.application.services.category.CategoryService;
import kz.attractor.application.services.category.SubCategoryService;
import kz.attractor.application.services.clubs.ClubBranchService;
import kz.attractor.application.services.clubs.ClubService;
import kz.attractor.application.utils.ClubMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClubServiceImpl implements ClubService {
    private final ClubRepository clubRepository;
    private final CategoryService categoryService;
    private final SubCategoryService subCategoryService;
    private final ClubBranchService clubBranchService;


    public ClubServiceImpl(ClubRepository clubRepository, CategoryService categoryService, SubCategoryService subCategoryService, ClubBranchService clubBranchService) {
        this.clubRepository = clubRepository;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
        this.clubBranchService = clubBranchService;
    }

    @Override
    public List<Club> getAllClubs() {
        return clubRepository.findAll();
    }

    @Override
    public List<GetClubDTO> getAllOrderByName() {
        List<Club> clubs = clubRepository.findAllByOrderByNameAsc();
        return clubs.stream().map(GetClubDTO::mapToClub).collect(Collectors.toList());
    }

    @Override
    public Club getClubById(Long id) {
        Optional<Club> club = clubRepository.findById(id);

        if (club.isEmpty())
        {
            throw new NoSuchElementException("Club with ID %s doesn't exist!".formatted(club));
        }

        return club.get();
    }

    @Transactional
    @Override
    public void add(CreateClubDTO createClubDto) {

        List<CreateBranchDTO> branchDtos = createClubDto.getBranches();

        List<Branch> branches = branchDtos.stream().map(ClubMapper::mapClubBranch).toList();

        Club club = Club.builder()
                .name(createClubDto.getName())
                .category(categoryService.getCategoryById(createClubDto.getCategoryID()))
                .build();

        if(createClubDto.getSubcategoryID() != null) {
            club.setSubcategory(subCategoryService.getSubcategoryById(createClubDto.getSubcategoryID()));
        }

        clubRepository.save(club);

        branches.forEach(branch -> clubBranchService.add(club, branch));
    }


    @Override
    public void delete(Long id) {
        List<ClubBranch> clubBranches = clubBranchService.findAllByClubId(id);
        clubBranches.forEach(cb -> clubBranchService.delete(cb.getId()));
        clubRepository.deleteById(id);
    }

    @Override
    public void addBranch(String clubId, CreateBranchDTO createBranchDTO) {
        Club club = getClubById(Long.valueOf(clubId));
        Branch branch = ClubMapper.mapClubBranch(createBranchDTO);

        clubBranchService.add(club, branch);
    }
}
