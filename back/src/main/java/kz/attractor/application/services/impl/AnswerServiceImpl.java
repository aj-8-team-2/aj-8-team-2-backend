package kz.attractor.application.services.impl;


import kz.attractor.application.DTO.AnswerDTO;
import kz.attractor.application.models.Answer;
import kz.attractor.application.models.AnswerCache;
import kz.attractor.application.models.Question;
import kz.attractor.application.models.User;
import kz.attractor.application.repositories.AnswerCacheRepository;
import kz.attractor.application.repositories.AnswerRepository;
import kz.attractor.application.repositories.QuestionRepository;
import kz.attractor.application.services.AnswerService;
import kz.attractor.application.services.UserService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class AnswerServiceImpl implements AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;
    private final UserService userService;
    private final AnswerCacheRepository answerCacheRepository;

    public AnswerServiceImpl(AnswerRepository answerRepository,
                             QuestionRepository questionRepository,
                             UserService userService,
                             AnswerCacheRepository answerCacheRepository) {
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
        this.userService = userService;
        this.answerCacheRepository = answerCacheRepository;
    }

    @Override
    public void save(AnswerDTO answerDTO) {
        Question question = questionRepository.findById(answerDTO.getQuestionId())
                .orElseThrow(() -> new IllegalArgumentException("Вопрос не найден"));

        String username = answerDTO.getUsername();
        User user = userService.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }

        Answer answer = Answer.builder()
                .user(user)
                .question(question)
                .text(answerDTO.getText())
                .createdAt(LocalDateTime.now())
                .build();

        try {
            answerCacheRepository.deleteById(answerDTO.getQuestionId().toString());
        } catch (Exception exception) {
            System.err.println("Ошибка при удалении данных из кэша: " + exception.getMessage());
        }

        answerRepository.save(answer);
    }

    @Override
    public List<AnswerDTO> findAnswersByQuestionId(Long questionId) {
        return answerCacheRepository.findById(questionId.toString())
                .map(AnswerCache::getAnswers)
                .orElseGet(() -> {
                    List<Answer> answers = answerRepository.findByQuestionId(questionId);
                    List<AnswerDTO> answerDTOs = answers.stream()
                            .map(answer -> new AnswerDTO(answer.getId(), answer.getUser().getUsername(), answer.getQuestion().getId(), answer.getText()))
                            .collect(Collectors.toList());

                    AnswerCache answerCache = new AnswerCache();
                    answerCache.setId(questionId.toString());
                    answerCache.setAnswers(answerDTOs);
                    answerCacheRepository.save(answerCache);

                    return answerDTOs;
                });
    }
}
