package kz.attractor.application.services;

import kz.attractor.application.models.Image;

public interface ImageService {
    String upload(Image image);

    void delete(String fileName);
}
