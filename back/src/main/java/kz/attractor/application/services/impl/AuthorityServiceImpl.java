package kz.attractor.application.services.impl;

import kz.attractor.application.models.Authority;
import kz.attractor.application.repositories.AuthorityRepository;
import kz.attractor.application.services.AuthorityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorityServiceImpl implements AuthorityService {
    private final AuthorityRepository repository;
    @Override
    public List<Authority> findAllByAuthority(String authority) {
        return repository.findAllByAuthority(authority);
    }
}
