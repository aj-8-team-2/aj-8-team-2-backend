package kz.attractor.application.services;

import kz.attractor.application.DTO.ReviewDTO;

import java.util.List;

public interface ReviewService {
    void addReview(ReviewDTO reviewDTO);
    List<ReviewDTO> findReviewsByBranchId(Long branchId);

    Double getAverageRatingForClub(Long clubId);
}
