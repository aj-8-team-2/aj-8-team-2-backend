package kz.attractor.application.services.clubs;

import kz.attractor.application.DTO.clubs.CreateBranchDTO;
import kz.attractor.application.DTO.clubs.CreateClubDTO;
import kz.attractor.application.DTO.clubs.GetClubDTO;
import kz.attractor.application.models.Club;

import java.util.List;

public interface ClubService {
    List<Club> getAllClubs();
    List<GetClubDTO> getAllOrderByName();
    Club getClubById(Long id);
    void add(CreateClubDTO createClubDTO);
    void delete(Long id);
    void addBranch(String clubId, CreateBranchDTO createBranchDTO);
}
