package kz.attractor.application.services;

import kz.attractor.application.models.Authority;

import java.util.List;

public interface AuthorityService {
    List<Authority> findAllByAuthority(String authority);
}
