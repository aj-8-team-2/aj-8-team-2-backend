package kz.attractor.application.services.impl;

import jakarta.transaction.Transactional;
import kz.attractor.application.models.Branch;
import kz.attractor.application.models.Child;
import kz.attractor.application.models.Enrollment;
import kz.attractor.application.repositories.ChildRepository;
import kz.attractor.application.repositories.EnrollmentRepository;
import kz.attractor.application.services.EnrollmentService;
import kz.attractor.application.services.clubs.BranchService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EnrollmentServiceImpl implements EnrollmentService {

    private final ChildRepository childRepository;
    private final EnrollmentRepository enrollmentRepository;
    private final BranchService branchService;

    public EnrollmentServiceImpl(ChildRepository childRepository, EnrollmentRepository enrollmentRepository, BranchService branchService) {
        this.childRepository = childRepository;
        this.enrollmentRepository = enrollmentRepository;
        this.branchService = branchService;
    }

    @Override
    public List<Branch> getClubsByChildId(Long childId) {
        return enrollmentRepository.findAllByChildId(childId).stream()
                .map(Enrollment::getBranch)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void enrollChildToClub(Long childId, Long branchId) {

        if (enrollmentRepository.existsByChildIdAndBranchId(childId, branchId)) {
            throw new RuntimeException("Этот ребенок уже записан в данный клуб");
        }

        Child child = childRepository.findById(childId)
                .orElseThrow(() -> new RuntimeException("Ребенок с ID " + childId + " не найден"));

        Branch branch = branchService.getBranchById(branchId);

        if (branch == null) {
            throw new UsernameNotFoundException("Клуб не найден");
        }

        Enrollment enrollment = new Enrollment();
        enrollment.setChild(child);
        enrollment.setBranch(branch);

        enrollmentRepository.save(enrollment);
    }
}
