package kz.attractor.application.services;

import kz.attractor.application.DTO.AdvertisementDTO;
import kz.attractor.application.DTO.ImageDTO;
import kz.attractor.application.models.Advertisement;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;
import java.util.Optional;

public interface AdvertisementService {
    List<Advertisement> getAllAdvertisements();

    Optional<Advertisement> getAdvertisementById(Long id);

    Advertisement createAdvertisement(AdvertisementDTO advertisementDTO);

    void addAdvertImage(ImageDTO imageDTO, Long id);

    Advertisement updateAdvertisement(Long id, Advertisement updatedAdvertisement) throws ChangeSetPersister.NotFoundException;

    void deleteAdvertisement(Long id);

    List<Advertisement> getAdvertisementsSortedByPriority();

//    boolean isAllowedToShow(Advertisement advertisement);

//    int getMaxShowCountForPriority(Priority priority);
}
