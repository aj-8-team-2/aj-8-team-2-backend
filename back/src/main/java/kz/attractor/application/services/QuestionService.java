package kz.attractor.application.services;

import kz.attractor.application.DTO.QuestionDTO;
import kz.attractor.application.models.Question;

import java.util.List;

public interface QuestionService {
    void save(QuestionDTO questionDTO);
    List<QuestionDTO> findByTopicIdOrderByCreatedAtDesc(Long topicId);
    List<Question> findQuestionsForModeration();
    void moderateQuestion(Long questionId);
    void deleteQuestion(Long questionId);
}
