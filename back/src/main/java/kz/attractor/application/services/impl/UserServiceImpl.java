package kz.attractor.application.services.impl;

import kz.attractor.application.DTO.users.CreateUserDTO;
import kz.attractor.application.DTO.users.UpdateUserDTO;
import kz.attractor.application.exceptions.DuplicateException;
import kz.attractor.application.exceptions.MaxDataException;
import kz.attractor.application.models.Authority;
import kz.attractor.application.models.User;
import kz.attractor.application.repositories.UserRepository;
import kz.attractor.application.services.AuthorityService;
import kz.attractor.application.services.EmailService;
import kz.attractor.application.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

import kz.attractor.application.models.Child;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AuthorityService authorityService;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;


    @Override
    public void create(CreateUserDTO createUserDTO) {
        List<User> existingUsers = userRepository.findAll();
        for(User user : existingUsers){
            if(user.getUsername().equals(createUserDTO.getUsername())){
                throw new DuplicateException("User already exists", 500);
            }
        }
        List<Authority> authorities = authorityService.findAllByAuthority("ROLE_USER");

        User user = User.builder()
                .username(createUserDTO.getUsername())
                .firstname(createUserDTO.getFirstname())
                .lastname(createUserDTO.getLastname())
                .phone(createUserDTO.getPhone())
                .authorities(authorities)
                .password(passwordEncoder.encode(createUserDTO.getPassword()))
                .build();
        userRepository.save(user);

        String subject = "Регистрация успешна";
        String text = "Вы успешно зарегистрировались. Ваш логин: " + createUserDTO.getUsername() +
                "Ваш пароль: " + createUserDTO.getPassword();
        emailService.sendEmail(createUserDTO.getUsername(), subject, text);
    }

    @Override
    public User findByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }
        return user.get();
    }

    @Override
    public List<Child> getAllChildrenByUserId(Long userId) {
        return userRepository.findAllById(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException("User with username %s doesn't exists!".formatted(username));
        }
        return user.get();
    }

    @Override
    public void updateUserInfo(String username, UpdateUserDTO updateUserDTO) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username %s doesn't exist!".formatted(username)));
        String newFirstname = updateUserDTO.getFirstname();
        if(!newFirstname.isEmpty() || !newFirstname.isBlank()){
            user.setFirstname(newFirstname);
        }

        String newLastname = updateUserDTO.getLastname();
        if(!newLastname.isEmpty() || !newLastname.isBlank()){
            user.setLastname(newLastname);
        }
        String newPhone = updateUserDTO.getPhone();
        if(!newPhone.isEmpty() || !newPhone.isBlank()){
            user.setPhone(newPhone);
        }
        user.setPhotoUrl(updateUserDTO.getPhotoUrl());

        userRepository.save(user);
    }

    @Override
    public void updateChildren(String username, Child child) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username %s doesn't exist!".formatted(username)));
        List<Child> childList = user.getChildren();
        if(childList == null || childList.isEmpty()){
            childList = new ArrayList<>();
        }
        if(childList.size() >= 10){
            throw new MaxDataException("Максимальное количество детей достигнуто");
        }

        childList.add(child);
        user.setChildren(childList);
        userRepository.save(user);
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new NoSuchElementException("User not found with ID: " + userId));
    }
}

