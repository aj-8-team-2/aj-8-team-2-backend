package kz.attractor.application.DTO.clubs;

import kz.attractor.application.models.Club;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetClubDTO {
    private Long id;
    private String name;

    public static GetClubDTO mapToClub(Club club) {
        return new GetClubDTO(club.getId(), club.getName());
    }
}
