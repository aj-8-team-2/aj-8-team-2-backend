package kz.attractor.application.DTO.users;

import kz.attractor.application.models.Authority;
import kz.attractor.application.models.Child;
import kz.attractor.application.models.User;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserDTO {
    private String username;
    private String firstname;
    private String lastname;
    private String phone;
    private List<Authority> roles;
    private List<Child> children;

    public static GetUserDTO map(User user) {
        return new GetUserDTO(user.getUsername(), user.getFirstname(), user.getLastname(),
                user.getPhone(), user.getAuthorities(), user.getChildren());
    }
}
