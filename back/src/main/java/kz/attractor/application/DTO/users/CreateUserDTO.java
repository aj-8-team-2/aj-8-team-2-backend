package kz.attractor.application.DTO.users;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserDTO {
    @NotBlank
    @Email
    private String username;

    @NotBlank
    private String firstname;

    private String lastname;

    @NotBlank
    private String phone;

    @NotBlank
    @Size(min = 4, max = 15)
    private String password;

}