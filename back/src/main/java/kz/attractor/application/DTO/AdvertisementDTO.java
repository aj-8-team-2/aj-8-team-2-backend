package kz.attractor.application.DTO;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisementDTO {

    private String name;
//    private String image;
    private String link;
    private Long priorityID;
    private List<String> images;
}

