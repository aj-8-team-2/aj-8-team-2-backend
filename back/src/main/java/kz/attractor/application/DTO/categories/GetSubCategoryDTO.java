package kz.attractor.application.DTO.categories;

import kz.attractor.application.models.SubCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetSubCategoryDTO {
    private Long id;

    private String name;

    public static GetSubCategoryDTO map(SubCategory subCategory) {
        return new GetSubCategoryDTO(subCategory.getId(), subCategory.getName());
    }

}
