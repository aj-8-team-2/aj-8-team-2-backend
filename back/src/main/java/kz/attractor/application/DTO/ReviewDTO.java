package kz.attractor.application.DTO;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDTO {
    private String username;
    private Long clubId;
    private String comment;
    private Integer rating;
    private String createdAt;
}
