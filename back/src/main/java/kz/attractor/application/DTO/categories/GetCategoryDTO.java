package kz.attractor.application.DTO.categories;

import kz.attractor.application.models.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetCategoryDTO {
    private Long id;

    private String name;

    private List<GetSubCategoryDTO> subcategories;

    public static GetCategoryDTO map(Category category) {
        return new GetCategoryDTO(category.getId(), category.getName(),
                category.getSubcategories().stream().map(GetSubCategoryDTO::map).collect(Collectors.toList()));
    }
}
