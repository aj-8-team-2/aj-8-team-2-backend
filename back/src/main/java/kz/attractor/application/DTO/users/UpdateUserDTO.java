package kz.attractor.application.DTO.users;

import kz.attractor.application.models.Child;
import lombok.*;

import java.util.List;
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUserDTO {
    private String firstname;
    private String lastname;
    private String phone;
    private String photoUrl;
}

