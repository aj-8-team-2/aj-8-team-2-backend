package kz.attractor.application.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.*;

@Slf4j
public class AuthorizationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (request.getServletPath().equals("/login") || request.getServletPath().equals("/tokens/refresh") || request.getServletPath().equals("/users")) {
            filterChain.doFilter(request, response);
        }
        else {
            try {
                String header = request.getHeader(HttpHeaders.AUTHORIZATION);

                if (header != null && header.startsWith("Bearer ")) {
                    String bearer = header.substring("Bearer ".length());
                    Algorithm algorithm = Algorithm.HMAC256("2f30ffcf-9987-4d1f-b9b2-c640b6c59860".getBytes());

                    JWTVerifier verifier = JWT
                                                .require(algorithm)
                                                .build();

                    DecodedJWT decodedJWT = verifier.verify(bearer);

                    String username = decodedJWT.getSubject();
                    String[] roles = decodedJWT
                                                .getClaim("authorities")
                                                .asArray(String.class);

                    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    Arrays.asList(roles).forEach(authority -> authorities.add(new SimpleGrantedAuthority(authority)));

                    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(token);

                    filterChain.doFilter(request, response);
                }
            }

            catch (Exception exception) {
                log.error("error", exception);
                String message = exception.getMessage();

                response.setHeader("error", message);
                response.setStatus(HttpStatus.FORBIDDEN.value());

                Map<String, String> errors = new HashMap<>();
                errors.put("error_message", message);

                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), errors);
            }
        }
    }
}
