package kz.attractor.application.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class MaxDataException extends RuntimeException {
    private final String message;
}
