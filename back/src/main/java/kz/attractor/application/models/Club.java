package kz.attractor.application.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Builder
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "clubs")
public class Club extends BaseEntity{

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    private SubCategory subcategory;

    private String name;

    @OneToMany(mappedBy = "club")
    private List<ClubBranch> clubBranches;
}