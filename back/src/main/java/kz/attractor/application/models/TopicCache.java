package kz.attractor.application.models;

import org.springframework.data.annotation.Id;
import kz.attractor.application.DTO.TopicDTO;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("TopicCache")
public class TopicCache {
    @Id
    private String id;
    private List<TopicDTO> topics;
}