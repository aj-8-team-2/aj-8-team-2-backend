package kz.attractor.application.models;

import org.springframework.data.annotation.Id;
import kz.attractor.application.DTO.AnswerDTO;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("AnswerCache")
public class AnswerCache {
    @Id
    private String id;
    private List<AnswerDTO> answers;
}