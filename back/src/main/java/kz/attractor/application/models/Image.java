package kz.attractor.application.models;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Image {
    private MultipartFile file;

}

