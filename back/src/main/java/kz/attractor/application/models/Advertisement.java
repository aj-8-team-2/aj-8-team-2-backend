package kz.attractor.application.models;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "advertisements")
public class Advertisement extends BaseEntity{

    private String name;
//    private String image;
    private String link;

    @ManyToOne
    @JoinColumn(name = "priority_id")
    private Priority priority;

    @Column(name = "image")
    @CollectionTable(name = "advertisement_images")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> images = new ArrayList<>();
}

