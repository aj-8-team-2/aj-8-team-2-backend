package kz.attractor.application.models;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter @Setter
@NoArgsConstructor
@Table(name = "test_free")
public class TestFree extends BaseEntity{
    private String title;

    @ElementCollection
    @CollectionTable(name = "test_free_results", joinColumns = @JoinColumn(name = "test_free_id"))
    @Column(name = "results")
    private List<String> results;

    @Column(name = "child_id")
    private Long childId;
}
