package kz.attractor.application.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
@Builder
@Entity
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "subcategories")
public class SubCategory extends BaseEntity{

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "subcategory", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Club> clubs;

}
