package kz.attractor.application.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "priorities")
@Entity
public class Priority extends BaseEntity {
    private String priority;

    @JsonIgnore
    @OneToMany(mappedBy = "priority", cascade = CascadeType.ALL)
    private List<Advertisement> advertisements;
}


