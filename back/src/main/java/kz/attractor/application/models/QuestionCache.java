package kz.attractor.application.models;

import org.springframework.data.annotation.Id;
import kz.attractor.application.DTO.QuestionDTO;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("QuestionCache")
public class QuestionCache {
    @Id
    private String id;
    private List<QuestionDTO> questions;
}