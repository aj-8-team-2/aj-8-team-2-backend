document.addEventListener('DOMContentLoaded', function () {
    getAdvertisementsSortedByPriority();
    $('#carouselExampleControls').carousel();
});

async function getAdvertisementsSortedByPriority() {
    try {
        const storedUser = localStorage.getItem("user");

        if (!storedUser) {
            console.error('User not logged in');
            return;
        }

        const token = JSON.parse(storedUser).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };

        const SERVER_URL_ADS = "http://localhost:8080/api/v1/advertisements";
        const response = await fetch(SERVER_URL_ADS + "/sortedByPriority", { headers });

        if (response.ok) {
            const advertisements = await response.json();
            displayAdvertisements(advertisements);
        } else {
            console.error('Request failed with status:', response.status);
        }
    } catch (error) {
        console.error('Error in request:', error);
    }
}

function displayAdvertisements(advertisements) {
    const adCarousel = document.getElementById('adCarousel');
    adCarousel.innerHTML = '';

    advertisements.forEach(ad => {
        const adElement = document.createElement('div');
        adElement.classList.add('carousel-item');

        const linkElement = document.createElement('a');
        linkElement.href = ad.link;
        linkElement.target = '_blank';

        const imgElement = document.createElement('img');
        imgElement.src = `http://localhost:9000/images/${ad.images}`;
        imgElement.alt = `Ad ID: ${ad.id}`;
        imgElement.classList.add('d-block', 'w-100');

        imgElement.style.width = '200px';
        imgElement.style.height = '350px';

        adElement.appendChild(linkElement);
        linkElement.appendChild(imgElement);

        adCarousel.appendChild(adElement);
    });

    if (advertisements.length > 0) {
        adCarousel.firstChild.classList.add('active');
    }

    $('#carouselExampleControls').carousel();
}