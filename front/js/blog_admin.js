'use strict';
const SERVER_URL = "http://localhost:8080/api/v1/";

$(document).ready(function() {
    fetchNews();
    $('#add-news-btn').on('click', function() {
        const title = $('#news-title-input').val();
        const content = $('#news-content-input').val();
        if (title && content) {
            addNews(title, content);
        } else {
            alert('Заголовок и содержание новости не могут быть пустыми.');
        }
    });
});

function fetchNews() {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    $.ajax({
        url: SERVER_URL + 'news',
        type: 'GET',
        headers: { 'Authorization': 'Bearer ' + token },
        dataType: 'json',
        success: function(newsList) {
            const newsContainer = $('#news-container');
            newsContainer.empty();
            newsList.forEach(function(news, index) {
                const newsHtml = `
                    <tr>
                        <th>${index + 1}</th>
                        <td>${news.title}</td>
                        <td>
                            <button class="btn btn-danger delete-news-btn" data-news-id="${news.id}">Удалить</button>
                        </td>
                    </tr>`;
                newsContainer.append(newsHtml);
            });

            $('.delete-news-btn').on('click', function() {
                const newsId = $(this).data('news-id');
                deleteNews(newsId);
            });
        },
        error: function() {
            $('#news-container').html('<tr><td colspan="2">Произошла ошибка при загрузке новостей.</td></tr>');
        }
    });
}

function deleteNews(newsId) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    $.ajax({
        url: SERVER_URL + 'news/' + newsId,
        headers: { 'Authorization': 'Bearer ' + token },
        type: 'DELETE',
        success: function() {
            alert('Новость успешно удалена');
            fetchNews();
        },
        error: function() {
            alert('Произошла ошибка при удалении новости.');
        }
    });
}

function addNews(title, content) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    $.ajax({
        url: SERVER_URL + 'news',
        type: 'POST',
        headers: { 'Authorization': 'Bearer ' + token },
        contentType: 'application/json',
        data: JSON.stringify({ title: title, content: content }),
        success: function() {
            alert('Новость успешно добавлена');
            fetchNews();
            $('#news-title-input').val('');
            $('#news-content-input').val('');
        },
        error: function() {
            alert('Произошла ошибка при добавлении новости.');
        }
    });
}