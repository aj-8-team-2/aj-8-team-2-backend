const API_URL_ADMIN = "http://localhost:8080/api/v1";

document.addEventListener('DOMContentLoaded', function () {
    let branchTableHeader = ["name", "address", "phoneNumber", "instagram", "service", "teachers", "schedule"];
    let BRANCH_KEYS = {
        name: "Название",
        address: "Адрес",
        phoneNumber: "Телефон",
        instagram: "Инстаграм",
        service: "Услуги",
        teachers: "Учителя/Тренера",
        schedule: "Расписание",
        mainBranch: "главный филиал"
    }
    let categoryList;
    let branchesList;
    let clubsList;

    const tabContentList = document.getElementsByClassName("tab-content");
    const tabLinks = document.getElementsByClassName("tab-links");
    const adminContainer = document.getElementById("adminContainer");
    const branchesTableContent = document.getElementById("branchesTableContent");
    const addClubForm = document.getElementById("addClubForm");
    const categoriesTab = document.getElementById("categoriesTab");
    const categoriesPane = document.getElementById("categoriesPane");
    const categoriesContent = document.getElementById("categoriesContent");
    const clubSelect = document.getElementById("clubSelect");
    const categorySelect = document.getElementById("categorySelect");
    const subcategorySelect = document.getElementById("subcategorySelect");
    const branchesTab = document.getElementById("branchesTab");
    const addClubTab = document.getElementById("addClubTab");
    const addBranchTab = document.getElementById("addBranchTab");
    const branchesPane = document.getElementById("branchesPane");
    const addClubPane = document.getElementById("addClubPane");
    const addBranchPane = document.getElementById("addBranchPane");
    const addCategoryBtn = document.getElementById("addCategoryBtn");

    const addBranchForm = document.getElementById("addBranchForm");

    const activeBranchesBtn = document.getElementById("activeBranchesBtn");
    const inactiveBranchesBtn = document.getElementById("inactiveBranchesBtn");
    const addAdvertTab = document.getElementById("addAdvertTab");
    const addAdvertPane = document.getElementById("addAdvertPane");

    function showMessage(type, text) {
        const messageContainer = document.getElementById('messageContainer');
        messageContainer.innerHTML = `<div class="alert alert-${type}" role="alert">${text}</div>`;

        setTimeout(() => {
            messageContainer.innerHTML = '';
        }, 5000);
    }


    addAdvertTab.addEventListener("click", e => {
        e.preventDefault();

        addClubTab.classList.remove("active")
        addBranchTab.classList.remove("active")
        branchesTab.classList.remove("active");
        categoriesTab.classList.remove("active");
        addAdvertTab.classList.add("active");

        for (let element of tabContentList) {
            element.classList.remove("isVisible")
        }
        addAdvertPane.classList.add("isVisible");
    })

    const advertisementForm = document.getElementById("advertisementForm");
    const addImageForm = document.getElementById("addImageForm");
    const imageForm = document.getElementById("imageForm");
    const advertisementIdField = document.getElementById("advertisementId");

    advertisementForm.addEventListener("submit", async e => {
        e.preventDefault();

        const name = document.getElementById('name').value;
        const link = document.getElementById('link').value;
        const priority = document.querySelector('input[name="priority"]:checked');

        if (!name || !link || !priority) {
            showMessage('error', 'Пожалуйста, заполните все поля.');
            return;
        }

        const token = JSON.parse(localStorage.getItem("user"))?.access_token;

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': token ? 'Bearer ' + token : '',
        };

        const data = {
            name,
            link,
            priorityID: priority ? priority.value : null,
        };

        try {
            const response = await axios.post(`${API_URL_ADMIN}/advertisements/create`, data, { headers });

            if (response?.status === 201) {
                showMessage('success', 'Реклама успешно создана');
                advertisementIdField.value = response.data.id;
                advertisementForm.style.display = 'none';
                addImageForm.style.display = 'block';
            } else {
                showMessage('error', `Error creating advertisement: ${response.status} - ${response.statusText}`);
            }
        } catch (error) {
            showMessage('error', `Error creating advertisement: ${error.message}`);
        }
    });

    imageForm.addEventListener("submit", async e => {
        e.preventDefault();

        const advertisementId = advertisementIdField.value;
        const imageFile = document.getElementById('imageFile').files[0];

        if (!imageFile) {
            showMessage('error', 'Пожалуйста, выберите изображение.');
            return;
        }

        const formData = new FormData();
        formData.append('advertisementId', advertisementId);
        formData.append('file', imageFile);

        const token = JSON.parse(localStorage.getItem("user"))?.access_token;

        const headers = {
            'Authorization': token ? 'Bearer ' + token : '',
            'Content-Type': 'multipart/form-data',
        };

        try {
            const response = await axios.post(`${API_URL_ADMIN}/advertisements/create/images/${advertisementId}`, formData, { headers });

            if (response?.status === 200) {
                alert("Изображение успешно добавлено к рекламе");
                refreshPage();
            } else {
                showMessage('error', `Error adding image to advertisement: ${response.status} - ${response.statusText}`);
            }
        } catch (error) {
            showMessage('error', `Error adding image to advertisement: ${error.message}`);
        }
    });

    async function refreshPage() {
        try {
            const delayTime = 1000; // 1 second
            await new Promise(resolve => setTimeout(resolve, delayTime));
            location.reload();
        } catch (error) {
            console.error('Error refreshing page:', error);
        }
    }


    async function loadPriorities() {
        try {
            const token = JSON.parse(localStorage.getItem("user"))?.access_token;

            const headers = {
                'Authorization': token ? 'Bearer ' + token : '',
            };

            const response = await fetch(`${API_URL_ADMIN}/priorities`, { headers });

            if (response.ok) {
                const priorities = await response.json();

                const priorityButtonsContainer = document.getElementById('priorityButtons');
                priorities.forEach(priority => {
                    const button = document.createElement('label');
                    button.className = 'btn btn-secondary';

                    let priorityLabel;

                    switch (priority.id) {
                        case 1:
                            priorityLabel = 'Низкий';
                            break;
                        case 2:
                            priorityLabel = 'Средний';
                            break;
                        case 3:
                            priorityLabel = 'Высокий';
                            break;
                        default:
                            priorityLabel = 'Unknown';
                    }

                    button.innerHTML = `<input type="radio" name="priority" value="${priority.id}" autocomplete="off"> ${priorityLabel}`;

                    priorityButtonsContainer.appendChild(button);
                });
            } else {
                console.error('Error loading priorities:', response.status, response.statusText);
            }
        } catch (error) {
            console.error('Error loading priorities:', error);
        }
    }

    async function getAllBranches(isActive) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.get(`${API_URL_ADMIN}/branches/filterByRelevance?active=${isActive}`, { headers });
            if (response?.status === 200) {
                console.log('r', response.data);
                branchesList = response.data;
                displayAllBranches(response.data);
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function getBranch(id) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.get(`${API_URL_ADMIN}/branches/${id}`, { headers });
            if (response?.status === 200) {
                const info = response.data;
                info.modalTitle = "Редактирование филиала";
                info.type = "branch";
                displayModal(info);
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function getAllClubs() {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.get(`${API_URL_ADMIN}/clubs`, { headers });
            if (response?.status === 200) {
                console.log('r', response.data);
                clubsList = response.data;
                fillClubOptions(response.data);
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function createClub(data) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.post(`${API_URL_ADMIN}/clubs`, data, { headers });
            if (response?.status === 200) {
                branchesList = response.data;
                displayAllBranches(response.data);
                refreshPage();
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function addBranchToClub(id, data) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.post(`${API_URL_ADMIN}/clubs/${id}/addBranch`, data, { headers });
            if (response?.status === 200) {
                alert(`Филиал ${data.name} был успешно сохранен!`);
                refreshPage();
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function editBranch(id, editData) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.put(`${API_URL_ADMIN}/branches/${id}`, editData, { headers });
            if (response?.status === 200) {
                alert("Филиал был изменен!");
                document.getElementById("blurBg").remove();
                getAllBranches(true);
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function deleteBranch(id) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.delete(`${API_URL_ADMIN}/branches/${id}`, { headers });
            if (response?.status === 200) {
                alert("Филиал был удален!");
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function deactivateBranch(id) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.put(`${API_URL_ADMIN}/branches/deactivate/${id}`, {}, { headers });
            if (response?.status === 200) {
                alert("Филиал был деактивирован!");
                getAllBranches(true);
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function getCategories() {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };

        try {
            const response = await axios.get(`${API_URL_ADMIN}/categories`, { headers });
            if (response?.status === 200) {
                categoryList = response.data;
                fillCategories(response.data);
            }
            return response;
        } catch (e) {
            return e;
        }
    }
    async function addCategory(data) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.post(`${API_URL_ADMIN}/categories`, data, { headers });
            if (response?.status === 200) {
                alert("Категория успешно добавлена!");
                document.getElementById("blurBg").remove();
                getCategories().then(() => displayAllCategories());
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function deleteCategory(id) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.delete(`${API_URL_ADMIN}/categories/${id}`, { headers });
            if (response?.status === 200) {
                alert("Категория была удален!");
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    async function addSubcategory(data) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.post(`${API_URL_ADMIN}/subcategories`, data, { headers });
            if (response?.status === 200) {
                alert("Подкатегория успешно добавлена!");
                document.getElementById("blurBg").remove();
                getCategories().then(() => displayAllCategories());
            }
            return response;
        } catch (e) {
            return e;
        }
    }
    async function deleteSubcategory(id) {
        const token = JSON.parse(localStorage.getItem("user")).access_token;
        const headers = {
            Authorization: "Bearer " + token,
        };
        try {
            const response = await axios.delete(`${API_URL_ADMIN}/subcategories/${id}`, { headers });
            if (response?.status === 200) {
                alert("Подкатегория была удален!");
            }
            return response;
        } catch (e) {
            return e;
        }
    }

    const displayAllBranches = () => {
        branchesTableContent.replaceChildren();
        if (branchesList?.length === 0) {
            branchesTableContent.innerHTML = ("Пока добавленных клубов нет");
        }
        else {
            for (let i = 0; i < branchesList.length; i++) {
                let counter = document.createElement("div");
                counter.innerHTML = (i+1).toString();
                let newBranchItem = document.createElement("div");
                newBranchItem.classList.add("branches-table-item");
                newBranchItem.append(counter);
                fillBranchItem(newBranchItem, branchesList[i]);
            }
        }
    }

    const createCategoryItem = (counter, data) => {
        let newContainer = document.createElement("div");
        newContainer.classList.add("category-container");

        const categoryHeader = document.createElement("div");
        categoryHeader.classList.add("category-header");

        let categoryName = document.createElement("div");
        categoryName.innerHTML = `${counter}. ${data.name}`
        categoryName.setAttribute("data-id", data.id);
        categoryName.classList.add("category-name");

        const subcategories = displaySubcategoriesList(data["subcategories"]);
        subcategories.classList.add("hidden");

        categoryName.addEventListener("click", e => {
            e.preventDefault();
            if (subcategories.classList.contains("hidden")) {
                subcategories.classList.remove("hidden");
            } else {
                subcategories.classList.add("hidden");
            }
        })

        const btnContainer = document.createElement("div");
        const addSubcategoryBtn = document.createElement("button");
        addSubcategoryBtn.innerHTML = "Добавить подкатегорию";
        addSubcategoryBtn.classList.add("btn", "category-btn");

        addSubcategoryBtn.addEventListener("click", e => {
            const info = {};
            info.modalTitle = "Добавление подкатегории";
            info.type = "subcategory";
            info.categoryId = data.id;
            info.categoryName = data.name;
            displayModal(info);

        })

        const deleteBtn = document.createElement("button");
        deleteBtn.innerHTML = "Удалить";
        deleteBtn.classList.add("btn", "category-btn", "category-btn-delete");

        deleteBtn.addEventListener("click", e => {
            e.preventDefault();
            deleteCategory(data.id);
        })

        btnContainer.append(addSubcategoryBtn, deleteBtn);

        categoryHeader.append(categoryName, btnContainer);
        newContainer.append(categoryHeader, subcategories);

        return newContainer;
    }

    addCategoryBtn.addEventListener("click", e => {
        e.preventDefault();
        const info = {};
        info.modalTitle = "Добавление категории";
        info.type = "category";
        displayModal(info);
    })

    const displayAllCategories = () => {
        categoriesContent.replaceChildren();
        if (categoryList?.length === 0) {
            categoriesContent.innerHTML = ("Пока добавленных категорий нет");
        }
        else {
            for (let i = 0; i < categoryList.length; i++) {
                let count = (i+1).toString();
                categoriesContent.append(createCategoryItem(count, categoryList[i]));
            }
        }
    }

    const createActionButtons = (itemId) => {
        let wrapper = document.createElement("div");
        wrapper.classList.add("action-btn-wrapper");
        wrapper.setAttribute("data-id", itemId);

        let branchDeleteBtn = document.createElement("span");
        branchDeleteBtn.classList.add("branch-delete-btn");

        let branchEditBtn = document.createElement("span");
        branchEditBtn.classList.add("branch-edit-btn");

        let branchDeactivateBtn = document.createElement("span");
        branchDeactivateBtn.classList.add("branch-deactivate-btn");

        wrapper.append(branchEditBtn, branchDeactivateBtn, branchDeleteBtn);

        branchDeleteBtn.addEventListener("click", e => {
            e.preventDefault();
            let parentEl = e.target.parentNode;
            if (parentEl) {
                deleteBranch(parentEl.getAttribute("data-id"));
            }
        })

        branchDeactivateBtn.addEventListener("click", e => {
            e.preventDefault();
            let parentEl = e.target.parentNode;
            if (parentEl) {
                deactivateBranch(parentEl.getAttribute("data-id"));
            }
        })

        branchEditBtn.addEventListener("click", e => {
            e.preventDefault();
            let parentEl = e.target.parentNode;
            if (parentEl) {
                getBranch(parentEl.getAttribute("data-id"));
            }
        })
        return wrapper;
    }

    activeBranchesBtn.addEventListener("click", e => {
        e.preventDefault();
        inactiveBranchesBtn.classList.remove("active-btn");
        activeBranchesBtn.classList.add("active-btn");
        branchesTableContent.replaceChildren();
        getAllBranches(true);
    })

    inactiveBranchesBtn.addEventListener("click", e => {
        e.preventDefault();
        inactiveBranchesBtn.classList.add("active-btn");
        activeBranchesBtn.classList.remove("active-btn");
        branchesTableContent.replaceChildren();
        getAllBranches(false);
    })

    const fillBranchItem = (newBranchItem, data) => {
        for (let i = 0; i < branchTableHeader.length; i++) { {
            Object.keys(data).forEach(key => {
                if (branchTableHeader[i] === key) {
                    let newDiv = document.createElement("div");
                    newDiv.innerHTML = data[key];
                    if (key === "name" && data['mainBranch'] === true) {
                        newDiv.classList.add("active")
                    }
                    newBranchItem.append(newDiv);
                }
            });
        }}
        newBranchItem.append(createActionButtons(data.id));
        branchesTableContent.append(newBranchItem);
    }

    const fillCategories = (categories) => {
        if (categories?.length > 0) {
            for (let category of categories) {
                let option = document.createElement("option");
                option.setAttribute("value", category.id);
                option.innerHTML = category.name;
                categorySelect.append(option);
            }
        }
    }

    const fillClubOptions = (clubs) => {
        if (clubs?.length > 0) {
            for (let club of clubs) {
                let option = document.createElement("option");
                option.setAttribute("value", club.id);
                option.innerHTML = club.name;
                clubSelect.append(option);
            }
        } else {
            addBranchForm.style.display = "none";
        }
    }

    const displaySubcategoriesList = (list) => {
        let newContainer = document.createElement("div");
        newContainer.style.marginTop = "10px";
        if (list.length > 0) {
            for (let i = 0; i < list.length; i++) {
                let newItem = document.createElement("div");
                let newSubcategory = document.createElement("div");

                let count = (i + 1).toString();
                newSubcategory.innerHTML = `${count}) ${list[i].name}`;
                newItem.classList.add("subcategory-item");

                const deleteBtn = document.createElement("button");
                deleteBtn.classList.add("btn", "subcategory-btn-delete");

                deleteBtn.addEventListener("click", e => {
                    e.preventDefault();
                    deleteSubcategory(list[i].id);
                })

                newItem.append(newSubcategory, deleteBtn);
                newContainer.append(newItem);
            }
        }
        return newContainer;
    }

    const fillSubCategories = (list) => {
        for (let subcategory of list) {
            let option = document.createElement("option");
            option.setAttribute("value", subcategory.id);
            option.innerHTML = subcategory.name;
            subcategorySelect.append(option);
        }
    }

    const createEditForm = (data) => {
        const newForm = document.createElement("form");

        Object.keys(BRANCH_KEYS).forEach(key => {
            let newDiv = document.createElement("div");
            newDiv.classList.add("edit-form-field");
            let newLabel = document.createElement("label");
            let newInput = document.createElement("input");
            let newTextarea = document.createElement("textarea");
            newInput.setAttribute("name", key);
            newTextarea.setAttribute("name", key);

            if (key === "mainBranch") {
                newInput.setAttribute("type", "checkbox");
                newLabel.innerHTML = BRANCH_KEYS[key];
                newInput.classList.add("form-check-input");
                newLabel.classList.add("form-check-label");
                newInput.checked = data[key];
                newDiv.classList.add("form-check");

                newDiv.append(newInput, newLabel);
            } else if (key === "service" || key === "teachers" || key === "schedule") {
                newTextarea.classList.add("form-control");
                newTextarea.value = data[key];
                newLabel.innerHTML = BRANCH_KEYS[key];
                newLabel.classList.add("form-label");
                newDiv.append(newLabel, newTextarea);
            }
            else {
                newLabel.classList.add("form-label");
                newInput.classList.add("form-control");
                newLabel.innerHTML = BRANCH_KEYS[key];
                newInput.value = data[key];
                newDiv.append(newLabel, newInput);
            }
            newForm.append(newDiv);
        })
        const button = document.createElement("button");
        button.innerHTML = ("Сохранить");
        button.setAttribute("type", "submit");
        button.classList.add("edit-btn-save");

        newForm.append(button)

        newForm.addEventListener("submit", e => {
            e.preventDefault();
            const editData = Object.values(newForm.elements).reduce((obj,field) => {
                if (field.name === "mainBranch") {
                    obj[field.name] = field.checked;
                } else {
                    obj[field.name] = field.value;
                }
                return obj
            }, {})

            editBranch(data.id, editData);
        })
        return newForm;
    }

    const createAddCategoryForm = () => {
        const newForm = document.createElement("form");

        let newDiv = document.createElement("div");
        newDiv.classList.add("edit-form-field");
        let newLabel = document.createElement("label");
        let newInput = document.createElement("input");

        newLabel.classList.add("form-label");
        newInput.classList.add("form-control");
        newLabel.innerHTML = "Название";

        newDiv.append(newLabel, newInput);

        const button = document.createElement("button");
        button.innerHTML = ("Сохранить");
        button.setAttribute("type", "submit");
        button.classList.add("edit-btn-save");

        newForm.append(newDiv, button);

        newForm.addEventListener("submit", e => {
            e.preventDefault();
            addCategory({name: newInput.value});
        })
        return newForm;
    }

    const createAddSubcategoryForm = (data) => {
        const categoryInfo = document.createElement("div");
        categoryInfo.innerHTML = data.categoryName;

        const newForm = document.createElement("form");

        let newDiv = document.createElement("div");
        newDiv.classList.add("edit-form-field");
        let newLabel = document.createElement("label");
        let newInput = document.createElement("input");

        newLabel.classList.add("form-label");
        newInput.classList.add("form-control");
        newLabel.innerHTML = "Название";

        newDiv.append(newLabel, newInput);

        const button = document.createElement("button");
        button.innerHTML = ("Сохранить");
        button.setAttribute("type", "submit");
        button.classList.add("edit-btn-save");

        newForm.append(categoryInfo, newDiv, button);

        newForm.addEventListener("submit", e => {
            e.preventDefault();
            addSubcategory({categoryId: data.categoryId, name: newInput.value});
        })
        return newForm;
    }

    const displayModal = (info) => {
        const blurBackground = document.createElement("div");
        blurBackground.classList.add("blur-bg");
        blurBackground.setAttribute("id", "blurBg");

        const container = document.createElement("div");
        container.classList.add("edit-modal");

        const title = document.createElement("div");
        title.classList.add("edit-modal-title");
        title.innerHTML = info.modalTitle;

        const closeButton = document.createElement("div");
        closeButton.classList.add("modal-close-button");

        title.append(closeButton);
        if (info.type === "branch") {
            container.append(title, createEditForm(info));
        }
        else if (info.type === "category") {
            container.append(title, createAddCategoryForm());
        }
        else {
            container.append(title, createAddSubcategoryForm(info));
        }
        blurBackground.append(container);

        closeButton.addEventListener("click", () => {
            blurBackground.remove();
        });

        adminContainer.append(blurBackground);
    }

    categorySelect.addEventListener("change", e => {
        subcategorySelect.replaceChildren();
        const list = categoryList.find(el => e.target.value == el.id);
        if (list) {
            fillSubCategories(list["subcategories"]);
        }
    })

    branchesTab.addEventListener("click", e => {
        e.preventDefault();
        branchesTableContent.replaceChildren();
        getAllBranches(true);

        addClubTab.classList.remove("active")
        addBranchTab.classList.remove("active")
        branchesTab.classList.add("active");
        categoriesTab.classList.remove("active");
        addAdvertTab.classList.remove("active");

        for (let element of tabContentList) {
            element.classList.remove("isVisible")
        }
        branchesPane.classList.add("isVisible");
    })

    addClubTab.addEventListener("click", e => {
        e.preventDefault();

        addClubTab.classList.add("active")
        addBranchTab.classList.remove("active")
        branchesTab.classList.remove("active");
        categoriesTab.classList.remove("active");
        addAdvertTab.classList.remove("active");

        for (let element of tabContentList) {
            element.classList.remove("isVisible")
        }
        addClubPane.classList.add("isVisible");
    })

    addBranchTab.addEventListener("click", e => {
        e.preventDefault();
        getAllClubs();

        addClubTab.classList.remove("active")
        addBranchTab.classList.add("active")
        branchesTab.classList.remove("active");
        categoriesTab.classList.remove("active");
        addAdvertTab.classList.remove("active");

        for (let element of tabContentList) {
            element.classList.remove("isVisible")
        }
        addBranchPane.classList.add("isVisible");
    })

    categoriesTab.addEventListener("click", e => {
        e.preventDefault();

        addClubTab.classList.remove("active")
        addBranchTab.classList.remove("active")
        branchesTab.classList.remove("active");
        categoriesTab.classList.add("active");
        addAdvertTab.classList.remove("active");

        for (let element of tabContentList) {
            element.classList.remove("isVisible")
        }
        categoriesPane.classList.add("isVisible");
        displayAllCategories();
    })

    addClubForm.addEventListener("submit", e => {
        e.preventDefault();
        const clubData = {};
        clubData.name = document.getElementById("clubName").value;
        clubData.categoryID = categorySelect.value;
        clubData.subcategoryID = subcategorySelect.value.length !== 0 ? subcategorySelect.value : null;
        clubData.branches = [];
        const newBranch =  {};
        newBranch.name = document.getElementById("branchName").value;
        newBranch.address = document.getElementById("address").value;
        newBranch.phoneNumber = document.getElementById("phoneNumber").value;
        newBranch.instagram = document.getElementById("instagram").value;
        newBranch.service = document.getElementById("service").value;
        newBranch.teachers = document.getElementById("teachers").value;
        newBranch.schedule = document.getElementById("schedule").value;
        newBranch.mainBranch = document.getElementById("mainBranchCheckbox").checked;
        clubData.branches.push(newBranch);

        createClub(clubData);
    })

    addBranchForm.addEventListener("submit", e => {
        e.preventDefault();
        const clubId = clubSelect.value;
        const newBranch = {};
        newBranch.name = document.getElementById("newBranchName").value;
        newBranch.address = document.getElementById("branchAddress").value;
        newBranch.phoneNumber = document.getElementById("branchPhoneNumber").value;
        newBranch.instagram = document.getElementById("branchInstagram").value;
        newBranch.service = document.getElementById("branchService").value;
        newBranch.teachers = document.getElementById("branchTeachers").value;
        newBranch.schedule = document.getElementById("branchSchedule").value;
        newBranch.mainBranch = false;

        addBranchToClub(clubId, newBranch);
    })

    loadPriorities();
    getAllBranches(true);
    getCategories();
});

















































