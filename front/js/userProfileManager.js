import { postUser } from "./API.js";

document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('addChildButton').addEventListener('click', function() {
        const childrenContainer = document.getElementById('childrenContainer');
        const childDiv = document.createElement('div');
        childDiv.className = 'child';

        const nameInput = document.createElement('input');
        nameInput.type = 'text';
        nameInput.name = 'childName';
        nameInput.placeholder = 'Имя ребенка';

        const ageInput = document.createElement('input');
        ageInput.type = 'number';
        ageInput.name = 'childAge';
        ageInput.placeholder = 'Возраст ребенка';

        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Удалить';
        deleteButton.onclick = function() {
            childDiv.remove();
        };

        childDiv.appendChild(nameInput);
        childDiv.appendChild(ageInput);
        childDiv.appendChild(deleteButton);
        childrenContainer.appendChild(childDiv);
    });

    const profileForm = document.getElementById('profileForm');
    profileForm.addEventListener('submit', function(event) {
        event.preventDefault();

        const formData = {
            firstname: document.getElementById('firstname').value,
            lastname: document.getElementById('lastname').value,
            gender: document.getElementById('gender').value,
            age: document.getElementById('age').value,
            phone: document.getElementById('phone').value,
            email: document.getElementById('email').value,
            children: Array.from(document.querySelectorAll('.child')).map(child => {
                return {
                    name: child.querySelector('[name="childName"]').value,
                    age: child.querySelector('[name="childAge"]').value
                };
            })
        };

        postUser(formData).then(response => {
            console.log('Данные пользователя успешно обновлены!');
        }).catch(error => {
            console.error('Ошибка при обновлении данных пользователя!', error);
        });
    });
});
