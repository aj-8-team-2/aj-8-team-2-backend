'use strict';
const SERVER_URL = "http://localhost:8080/api/v1/";

$(document).ready(function () {
    loadChildren();
    const clubDetails = JSON.parse(localStorage.getItem('clubDetails'));
    if (clubDetails) {
        document.getElementById('clubDetails').innerHTML = `
        <div class="container mt-5">
            <h1 class="mb-4">${clubDetails.name}</h1>
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-2"><strong>Адрес:</strong> ${clubDetails.address}</p>
                    <p class="mb-2"><strong>Телефон:</strong> ${clubDetails.phoneNumber}</p>
                    <p class="mb-2"><strong>Расписание:</strong> ${clubDetails.schedule}</p>
                    <p class="mb-2"><strong>Инстаграм:</strong> <a href="${clubDetails.instagram}" target="_blank">${clubDetails.instagram}</a></p>
                </div>
            </div>
        </div>
    `;
        loadAndDisplayReviews(clubDetails.id);
    }

    $('#reviewForm').submit(function (event) {
        event.preventDefault();

        if (!clubDetails || !clubDetails.id) {
            alert('Ошибка: информация о клубе не найдена.');
            return;
        }

        const token = JSON.parse(localStorage.getItem("user"))?.access_token;
        const username = JSON.parse(localStorage.getItem("user"))?.username;
        const formData = {
            username: username,
            clubId: clubDetails.id,
            comment: $('#reviewText').val(),
            rating: $('#reviewRating').val()
        };

        $.ajax({
            type: "POST",
            url: SERVER_URL + "reviews/add",
            contentType: "application/json",
            headers: { 'Authorization': 'Bearer ' + token },
            data: JSON.stringify(formData),
            success: function (response) {
                alert('Ваш отзыв успешно отправлен!');
                $('#reviewForm').trigger("reset");
                loadAndDisplayReviews(clubDetails.id);
            },
            error: function () {
                alert('Произошла ошибка при отправке отзыва.');
            }
        });
    });

    $('#enrollmentForm').submit(function(event) {
        event.preventDefault();

        const selectedChildId = $('#childSelect').val();
        const clubDetails = JSON.parse(localStorage.getItem('clubDetails'));
        const token = JSON.parse(localStorage.getItem("user"))?.access_token;

        if (!clubDetails || !clubDetails.id || !selectedChildId) {
            alert('Ошибка: необходимо выбрать ребенка и клуб.');
            return;
        }

        const enrollmentData = {
            clubId: clubDetails.id,
            childId: selectedChildId
        };

        $.ajax({
            type: "POST",
            url: SERVER_URL + "enrollments",
            contentType: "application/json",
            headers: { 'Authorization': 'Bearer ' + token },
            data: JSON.stringify(enrollmentData),
            success: function(response) {
                alert('Ребенок успешно записан в клуб!');
            },
            error: function(xhr) {
                alert(xhr.responseText);
            }
        });
    });
});

function loadAndDisplayReviews(clubId) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    $.ajax({
        type: "GET",
        url: SERVER_URL + "reviews/club/" + clubId,
        headers: { 'Authorization': 'Bearer ' + token },
        contentType: "application/json",
        success: function (reviews) {
            if (reviews && reviews.length > 0) {
                const reviewsHtml = reviews.map(review =>
                    `<div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">${review.username} - ${review.createdAt}</h5>
                        <p class="card-text">${review.comment}</p>
                    </div>
                </div>`).join('');
                $('#reviewsContainer').html(reviewsHtml);
            } else {
                $('#reviewsContainer').html('<p>Пока что нет отзывов о клубе.</p>');
            }
        },
        error: function () {
            alert('Произошла ошибка при загрузке отзывов.');
        }
    });
}

function loadChildren() {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    const username = JSON.parse(localStorage.getItem("user"))?.username;

    $.ajax({
        type: "GET",
        url: SERVER_URL + "children/byUser/" + encodeURIComponent(username),
        headers: { 'Authorization': 'Bearer ' + token },
        contentType: "application/json",
        success: function(children) {
            const childSelect = $('#childSelect');
            childSelect.empty();
            if (children && children.length > 0) {
                children.forEach(child => {
                    childSelect.append(`<option value="${child.id}">${child.firstname} ${child.lastname}</option>`);
                });
            } else {
                childSelect.append('<option disabled>Нет доступных детей</option>');
            }
        },
        error: function() {
            alert('Произошла ошибка при загрузке списка детей.');
        }
    });
}




