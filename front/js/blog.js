'use strict';
const SERVER_URL = "http://localhost:8080/api/v1/";

$(document).ready(function() {
    fetchNews();
});

function fetchNews() {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    $.ajax({
        url: SERVER_URL + 'news',
        type: 'GET',
        headers: { 'Authorization': 'Bearer ' + token },
        dataType: 'json',
        success: function(newsList) {
            $('#news-container').empty();
            newsList.forEach(function(news, index) {
                const createdAt = new Date(news.createdAt);
                const formattedDate = createdAt.toLocaleDateString('ru-RU');
                const newsHtml = `
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title" id="title-${index}" style="cursor: pointer;">${news.title}</h5>
                            <p class="card-text" id="content-${index}" style="display: none;">${news.content}</p>
                            <p class="card-text"><small class="text-muted">${formattedDate}</small></p>
                        </div>
                    </div>`;
                $('#news-container').append(newsHtml);

                $(`#title-${index}`).on('click', function() {
                    $(`#content-${index}`).toggle();
                });
            });
        },
        error: function() {
            $('#news-container').html('<p>Произошла ошибка при загрузке новостей.</p>');
        }
    });
}
