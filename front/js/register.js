import {postUser} from "./API.js";

const isValidateFirstnameOrLastname = (firstNameOrLastname) => {
    return String(firstNameOrLastname)
        .match(
            /^[A-Za-zА-Яа-яЁё]+$/
        );
};

const isValidateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

const isValidatePassword = (password) => {
    return String(password)
        .match(
            /^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z]{3,14}$/
        );
};

 async function register(event) {

    const lastname = $('#newLastname').val()
    if(!isValidateFirstnameOrLastname(lastname)){
        alert('Пожалуйста, введите корректную фамилию.');
        return;
    }

    const firstname = $('#newFirstname').val()
    if(!isValidateFirstnameOrLastname(firstname)){
        alert('Пожалуйста, введите корректное имя.');
        return;
    }

    const email = $('#newUsername').val();
    if(!isValidateEmail(email)){
        alert('Пожалуйста, введите корректный адрес электронной почты.');
        return;
    }


    const phone = $('#newPhone').val();

    const password = $('#newPassword').val();
    if(!isValidatePassword(password)){
        alert('Пожалуйста, введите корректный пароль.');
        return;
    }

    const formData = {
        "firstname": firstname,
        "lastname": lastname,
        "username": email,
        "phone": phone,
        "password": password,
    };

    try {
        const response = await postUser(formData);
        if (response.success) {
            alert('Пользователь успешно сохранен, логин и пароль направлены на почту: ' + email);
        }
    } catch (error) {
        console.log(error);
    }
}

$(document).ready(function () {

    $("#newPhone").mask("+7 (999) 999 99-99");

    $('#togglePassword').click(function(){
        const passwordField = $('#newPassword');
        const fieldType = passwordField.attr('type');
        if (fieldType === 'password') {
            passwordField.attr('type', 'text');
            $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            passwordField.attr('type', 'password');
            $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });

    $("#registrationForm").on("submit", function(event)  {
        event.preventDefault();
        if (!$("#agreeCheckbox").is(":checked")) {
            alert("Вы должны согласиться на обработку персональных данных!");
        }
        else {
            register().then((res) => console.log(res)).catch((err) => {
                console.log('err', err)
            });
        }
    });

    document.getElementById("agreeCheckbox").addEventListener("change", function() {
        document.getElementById("registerButton").disabled = !this.checked;
    });

    document.getElementById("showTermsLink").addEventListener("click", function(event) {
        event.preventDefault();
        let termsText = document.getElementById("termsText");
        if (termsText.style.display === "none") {
            termsText.style.display = "block";
        } else {
            termsText.style.display = "none";
        }
    });
});