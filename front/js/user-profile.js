import {getUser, getTestFreeResult, updateUserInfo} from "./API.js";

$(document).ready(function() {
    const isValidateFirstnameOrLastname = (firstnameOrLastname) => {
        return /^[A-Za-zА-Яа-яЁё]*$/.test(firstnameOrLastname);
    };

    function getUserData() {
        let userData = JSON.parse(localStorage.getItem("user"));
        let username = userData.username
        getUser(username)
            .then(function (response) {
                console.log(response);
                let firstname = response.firstname;
                let lastname = response.lastname;

                $('#firstName').text(firstname);
                $('#firstNameHeader').text(firstname);
                $('#lastName').text(lastname);

                let tableHtml = '<table>';
                tableHtml += '<tr><th>№</th><th>ФИО</th><th>Пол</th><th>Возраст</th><th></th><th></th></tr>';
                response.children.sort(function(a, b) {
                    return a.id - b.id;
                });
                $.each(response.children, function(index, child) {
                    const currentYear = new Date().getFullYear();
                    const childAge = currentYear - child.birthYear;
                    tableHtml += '<tr>';
                    tableHtml += '<td>' + (index + 1) + '</td>';
                    tableHtml += '<td><a href="child-profile.html?childId=' + child.id + '">' + child.firstname + " " + child.lastname + '</a></td>';
                    tableHtml += '<td>' + child.gender + '</td>';
                    tableHtml += '<td>' + childAge + '</td>';
                    tableHtml += '<td style="cursor: pointer; color: blue; text-decoration: underline;" onclick="redirectToFreeTest(' + child.id + ')">Пройти тест</td>';
                    tableHtml += '<td><button class="resultBtn" onclick="getFreeTestResults(' + child.id + ')">Результаты тестирования</button></td>';
                    tableHtml += '</tr>';
                });
                tableHtml += '</table>';
                $('#childrenListContainer').html(tableHtml);

                const updateUserInfoContainer = $("#updateUserInfoContainer");
                const updateUserInfoLink = $("<a>").attr("href", "#").text("Редактировать личные данные");
                updateUserInfoContainer.append(updateUserInfoLink);

                updateUserInfoLink.on("click", function(event) {
                    event.preventDefault();
                    const form = $("<form>");

                    const firstnameInput = $("<input>").attr("type", "text").attr("placeholder", "Имя");
                    const lastnameInput = $("<input>").attr("type", "text").attr("placeholder", "Фамилия");
                    const phone = $("<input>").attr("type", "tel").attr("placeholder", "Телефон").attr("data-validate-field", "userNewPhone").attr("id", "userNewPhone");
                    const submitButton = $("<input>").attr("type", "submit").val("Добавить");

                    form.append(firstnameInput, lastnameInput, phone, submitButton);

                    updateUserInfoContainer.empty().append(form);

                    $("#userNewPhone").mask("+7 (999) 999 99-99");

                    submitButton.on("click", async function(event) {
                        event.preventDefault();
                        let userNewFirstnameInputValue = firstnameInput.val();
                        if(!isValidateFirstnameOrLastname(userNewFirstnameInputValue)){
                            alert('Пожалуйста, введите корректное имя.');
                            return;
                        }

                        let userNewLastnameInputValue = lastnameInput.val();
                        if(!isValidateFirstnameOrLastname(userNewLastnameInputValue)){
                            alert('Пожалуйста, введите корректную фамилию.');
                            return;
                        }
                        let userNewPhone = phone.val();
                        const formData = {
                            firstname: userNewFirstnameInputValue,
                            lastname: userNewLastnameInputValue,
                            phone: userNewPhone,
                        };
                        updateUserInfo(username, formData);
                    });
                });

            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.error("Error:", errorThrown);
            });
    }

    getUserData();
    window.getFreeTestResults = getFreeTestResults;
    window.redirectToFreeTest = redirectToFreeTest;
});

window.redirectToFreeTest =  function (childId){
    try {
        window.location.replace("test-free.html?childId=" + childId);
        event.preventDefault();
    } catch (error) {
        console.error('Error getting tests:', error);
    }
}

window.getFreeTestResults = function (childId) {
    try {
        getTestFreeResult(childId)
            .then(function(response) {
                console.log("Результаты тестирования:", response);
                let modal = $('<div class="modal">');
                let modalContent = $('<div class="modal-content">');
                let closeBtn = $('<span class="close">&times;</span>');
                closeBtn.on('click', function() {
                    modal.hide();
                });

                let resultsList = $('<ul>');
                if (Array.isArray(response)) {
                    response.forEach(function(testFree) {
                        testFree.results.forEach(function(result) {
                            resultsList.append('<li>' + result + '</li>');
                        });
                    });
                } else if (response.results) {
                    response.results.forEach(function(result) {
                        resultsList.append('<li>' + result + '</li>');
                    });
                } else {
                    console.error('Invalid response format:', response);
                    return;
                }
                modalContent.append('<h2>Результаты тестирования</h2>');
                modalContent.append(resultsList);
                modalContent.append(closeBtn);
                modal.append(modalContent);
                $('body').append(modal);
                modal.show();
            })
            .catch(function(error) {
                console.error('Error getting test results:', error);
            });
    } catch (error) {
        console.error('Error:', error);
    }
};