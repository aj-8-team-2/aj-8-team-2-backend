import {loginUser} from "./API.js";

const isValidateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

const isValidatePassword = (password) => {
    return String(password)
        .match(
            /^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z]{3,14}$/
        );
};


window.login = async function () {
    const username = $('#username').val();
    const password = $('#password').val();

    if(!isValidateEmail(username)){
        alert('Пожалуйста, введите корректный адрес электронной почты.');
        return;
    }

    if(!isValidatePassword(password)){
        alert('Пожалуйста, введите корректный пароль.');
        return;
    }

    let formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    try {
        await loginUser(formData);
    } catch (error) {
        console.log(error);
    }
};

$(document).ready(async function () {
    $('#togglePassword').click(function(){
        const passwordField = $('#password');
        const fieldType = passwordField.attr('type');
        if (fieldType === 'password') {
            passwordField.attr('type', 'text');
            $(this).find('i').removeClass('fa-eye').addClass('fa-eye-slash');
        } else {
            passwordField.attr('type', 'password');
            $(this).find('i').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });

    window.login = login;
});
