'use strict';
const SERVER_URL = "http://localhost:8080/api/v1/";

$(document).ready(function () {

    let refreshIntervalId = null;
    const refreshInterval = 20000;
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    function setRefreshInterval(topicId) {
        if (refreshIntervalId !== null) {
            clearInterval(refreshIntervalId);
        }
        refreshIntervalId = setInterval(() => loadQuestions(topicId), refreshInterval);
    }

    $.ajax({
        url: SERVER_URL + 'topics',
        type: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        },
        success: function(topics) {
            let $select = $("#topicSelect");
            topics.forEach(function(topic) {
                $select.append($('<option>', {
                    value: topic.id,
                    text: topic.name
                }));
            });
        }
    });

    $('#topicSelect').change(function () {
        const topicId = $(this).val();
        if (topicId) {
            $('#newQuestionForm').show();
            loadQuestions(topicId);
            setRefreshInterval(topicId);
            $(this).closest('div').hide();
            $('#title').hide();
            $('#questionsContainer').show();
        } else {
            $('#newQuestionForm').hide();
        }
    });

    $('#submitQuestion').click(function () {
        const token = JSON.parse(localStorage.getItem("user"))?.access_token;
        const username = JSON.parse(localStorage.getItem("user"))?.username;
        const questionText = $('#newQuestionText').val().trim();
        const topicId = $('#topicSelect').val();

        if (!questionText) {
            alert('Пожалуйста, введите текст вопроса.');
            return;
        }

        const questionData = {
            username: username,
            topicId: topicId,
            text: questionText
        };

        $.ajax({
            url: SERVER_URL + 'questions',
            type: 'POST',
            contentType: 'application/json',
            headers: { 'Authorization': 'Bearer ' + token },
            data: JSON.stringify(questionData),
            success: function () {
                alert("Ваш вопрос успешно добавлен.");
                $('#newQuestionText').val('');
                loadQuestions(topicId);
                setRefreshInterval(topicId);
            },
            error: function (xhr, status, error) {
                alert("Произошла ошибка при отправке вопроса: " + error);
            }
        });
    });

    $('#backToTopics').click(function () {
        $('#questionsContainer').hide();
        $('#newQuestionForm').hide();
        $('#topicSelect').val('').closest('div').show();
        $('#title').show();
    });
});

function submitAnswer(questionId, answerText, $answersList) {

    const token = JSON.parse(localStorage.getItem("user"))?.access_token;
    const username = JSON.parse(localStorage.getItem("user"))?.username;

    if (!answerText.trim()) {
        alert('Пожалуйста, введите ответ.');
        return;
    }

    const answerData = {
        username: username,
        questionId: questionId,
        text: answerText
    };

    $.ajax({
        url: SERVER_URL + 'answers',
        type: 'POST',
        contentType: 'application/json',
        headers: { 'Authorization': 'Bearer ' + token },
        data: JSON.stringify(answerData),
        success: function () {
            alert("Ваш ответ успешно отправлен!");
            $answersList.append($('<li>').addClass('answer-item').text(answerText));
        },
        error: function (xhr, status, error) {
            alert("Произошла ошибка при отправке ответа: " + error);
        }
    });
}

function loadQuestions(topicId) {

    const token = JSON.parse(localStorage.getItem("user"))?.access_token;

    $.ajax({
        url: SERVER_URL + 'questions/' + topicId,
        type: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        },
        success: function(questions) {
            let $questionsList = $('#questionsList');
            $questionsList.empty();

            $.each(questions, function (index, question) {
                let $questionContainer = $('<div>').addClass('question-container');
                let $questionTitle = $('<h5>').css('cursor', 'pointer').text(question.text);
                let $toggleIcon = $('<i>').addClass('fa fa-chevron-right').css('margin-right', '5px');

                $questionTitle.prepend($toggleIcon);

                let $responsesContainer = $('<div>').css('display', 'none');
                let $answersList = $('<ul>').attr('id', 'answersList' + question.id);

                $responsesContainer.append($answersList);

                let $answerInput = $('<textarea>').addClass('form-control custom-textarea').attr({
                    'rows': '2',
                    'placeholder': 'Введите ваш ответ здесь...',
                    'id': 'answerText' + question.id
                });
                $responsesContainer.append($answerInput);

                let $submitAnswerButton = $('<button>').addClass('btn btn-light btn-sm mt-2').text('Ответить').click(function () {
                    submitAnswer(question.id, $answerInput.val(), $answersList);
                    $answerInput.val('');
                });
                $responsesContainer.append($submitAnswerButton);

                $questionTitle.click(function () {
                    if ($answersList.children().length === 0) {

                        $.ajax({
                            url: SERVER_URL + 'answers/question/' + question.id,
                            type: 'GET',
                            headers: {
                                'Authorization': 'Bearer ' + token
                            },
                            success: function(answers) {
                                $.each(answers, function(index, answer) {
                                    $answersList.append($('<li>').addClass('answer-item').text(answer.text));
                                });
                                $responsesContainer.toggle();
                                $toggleIcon.toggleClass('fa-chevron-right fa-chevron-down');
                            }
                        });
                    } else {
                        $responsesContainer.toggle();
                        $toggleIcon.toggleClass('fa-chevron-right fa-chevron-down');
                    }
                });

                $questionContainer.append($questionTitle);
                $questionsList.append($questionContainer).append($responsesContainer);
            });
        },
        error: function() {
            alert("Ошибка загрузки вопросов для темы: " + topicId);
        }
    });
}
