$(document).ready(function () {
    const loginButtons = $("#loginButtons");
    const loggedUser = JSON.parse(localStorage.getItem("user")).username;
    if (loggedUser) {
        const newDiv = $("<div class='text-end'>");
        newDiv.text(loggedUser);
        $("#navbar").append(newDiv);
        loginButtons.hide();
    }

});