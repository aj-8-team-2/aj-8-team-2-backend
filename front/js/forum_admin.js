'use strict';
const SERVER_URL = "http://localhost:8080/api/v1/";

$(document).ready(function () {
    window.moderateQuestion = moderateQuestion;
    loadQuestionsForModeration();
});

function loadQuestionsForModeration() {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;

    $.ajax({
        url: `${SERVER_URL}questions/moderation`,
        type: 'GET',
        headers: {
            'Authorization': 'Bearer ' + token
        },
        success: function(questions) {
            const $questionsList = $('#questionsList');
            $questionsList.empty();
            questions.forEach((question, index) => {
                let row = `<tr>
                               <th scope="row">${index + 1}</th>
                               <td>${question.text}</td>
                               <td>
                                   <button class="btn btn-success" onclick="moderateQuestion(${question.id})">Отмодерировать</button>
                                    <button class="btn btn-danger" onclick="deleteQuestion(${question.id})">Удалить</button>
                               </td>
                           </tr>`;
                $questionsList.append(row);
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.error('Ошибка:', errorThrown);
        }
    });
}

function moderateQuestion(questionId) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;

    $.ajax({
        url: `${SERVER_URL}questions/${questionId}/moderate`,
        type: 'PUT',
        headers: {
            'Authorization': 'Bearer ' + token
        },
        success: function() {
            alert('Вопрос успешно отмодерирован');
            loadQuestionsForModeration();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Ошибка при модерации вопроса');
            console.error('Ошибка при модерации вопроса:', errorThrown);
        }
    });
}

function deleteQuestion(questionId) {
    const token = JSON.parse(localStorage.getItem("user"))?.access_token;

    $.ajax({
        url: `${SERVER_URL}questions/${questionId}/delete`,
        type: 'DELETE',
        headers: {
            'Authorization': 'Bearer ' + token
        },
        success: function() {
            alert('Вопрос успешно удалён');
            loadQuestionsForModeration();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert('Ошибка при удалении вопроса');
            console.error('Ошибка при удалении вопроса:', errorThrown);
        }
    });
}

